<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../demo.css">

    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/jquery.easyui.min.js"></script>
    <script src="../js/jquery.easyui.mobile.js"></script>
    <script src="../js/easyui-lang-ru.js"></script>
    <script>

    </script>

</head>
<script>
    $(document).ready(function () {
        $('#closabale_div').hide();
        fillFormConsumer();
    })
    ;

    function fillFormConsumer() {
        $.getJSON('/get_consumer_infodb/', function (data) {
                if (data.length === 1) {
                    var consumerInfo = data[0];
                    if (consumerInfo.LEGAL_ID === 0) {
                        return;
                    }
                    $('#legal_id').textbox('setText', consumerInfo.LEGAL_ID);
                    $('#guid').textbox('setText', consumerInfo.GUID);
                    $('#consumer_id').textbox('setText', consumerInfo.CONSUMER_ID);
                    $('#surname').textbox('setText', consumerInfo.SURNAME);
                    $('#firstName').textbox('setText', consumerInfo.FIRST_NAME);
                    $('#thirdName').textbox('setText', consumerInfo.THIRD_NAME);
                    $('#loginBigServ').textbox('setText', consumerInfo.LOGIN_BIG_SERV);
                    $('#closabale_div').show();
                }
            }
        );

    }
</script>
<body>
<h2>Форма регистрации</h2>
<div style="height: 800px; width: auto; display: flex; align-items: center; justify-content: center;">
    <div class="easyui-panel" title="New Topic" style="width:100%;max-width:1000px;padding:30px 60px;">
        <form id="ff" method="post">

            <div style="margin-bottom:10px">
                <input class="easyui-textbox" name="legal_id" id="legal_id" style="width:100%"
                       data-options="labelWidth:200,label:'Номер договора:', required:true">
            </div>
            <div style="margin-bottom:10px">
                <input class="easyui-textbox" name="guid" id="guid" style="width:100%"
                       data-options="labelWidth:200,label:'GUID:', required:true">
            </div>
        </form>
        <div id="closabale_div">
            <div style="margin-bottom:10px">
                <input class="easyui-textbox" name="consumer_id" id="consumer_id" style="width:100%"
                       data-options="labelWidth:200,label:'Персональный идентификатор:',readonly:true">
            </div>

            <div style="margin-bottom:10px">
                <input class="easyui-textbox" name="surname" id="surname" style="width:100%"
                       data-options="labelWidth:200,label:'Фамилия:',readonly:true">
            </div>

            <div style="margin-bottom:10px">
                <input class="easyui-textbox" name="firstName" id="firstName" style="width:100%"
                       data-options="labelWidth:200,label:'Имя:',readonly:true">
            </div>

            <div style="margin-bottom:10px">
                <input class="easyui-textbox" name="thirdName" id="thirdName" style="width:100%"
                       data-options="labelWidth:200,label:'Отчество:',readonly:true">
            </div>

            <div style="margin-bottom:10px">
                <input class="easyui-textbox" name="loginBigServ" id="loginBigServ" style="width:100%"
                       data-options="labelWidth:200,label:'Ваш логин на сервере:',readonly:true">
            </div>
        </div>
        <div style="text-align:center;padding:5px 0">
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:150px">Подтвердить регистрацию</a>
        </div>
    </div>
</div>
<script>
    function submitForm() {
        var postParam = {'legal_id': $('#legal_id').textbox('getText'), 'guid': $('#guid').textbox('getText')};
        var url = '/fromBigServer.html';
        $.post(url, postParam, function (data) {
            var newInf = JSON.parse(data);
            if (newInf != null && newInf.length === 1) {
                var consumerInfo = newInf[0];
                $.post('/set_consumer_infodb', {newUserConfig: JSON.stringify(newInf)})
                    .done(function (data) {
                        $('#closabale_div').show();
                        fillFormConsumer();
                    });
            }
        })

    }
</script>
</body>
</html>