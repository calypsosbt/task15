<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%!
    String getFormattedDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        return sdf.format(new Date());
    }
%>
<link rel="stylesheet" type="text/css" href="../themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../themes/icon.css">
<link rel="stylesheet" type="text/css" href="../demo.css">

<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/jquery.easyui.min.js"></script>
<script src="../js/jquery.easyui.mobile.js"></script>
<script src="../js/easyui-lang-ru.js"></script>
<script>
    function moveToUserPage() {
        var login = $('#login').textbox('getText');
        var pass = $('#pass').textbox('getText');
        if (login != 'admin' && pass != 'admin') return;

        location.href = '/logintoserv/'
    }
</script>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Добро пожаловать в умный дом!</title>
</head>
<body style="height: 800px; width: auto; display: flex; align-items: center; justify-content: center;">
<div class="easyui-panel" title="Login to system" style="width:100%;max-width:400px;padding:30px 60px; ">
    <div style="margin-bottom:10px">
        <input class="easyui-textbox" style="width:100%;height:40px;padding:12px" id="login"
               data-options="prompt:'Username',iconCls:'icon-man',iconWidth:38">
    </div>
    <div style="margin-bottom:20px">
        <input class="easyui-textbox" type="password" style="width:100%;height:40px;padding:12px" id="pass"
               data-options="prompt:'Password',iconCls:'icon-lock',iconWidth:38">
    </div>
    <div style="margin-bottom:20px">
        <input type="checkbox" checked="checked">
        <span>Remember me</span>
    </div>
    <div>
        <a class="easyui-linkbutton" data-options="iconCls:'icon-ok',
                                                   onClick: moveToUserPage"
           style="padding:5px 0px;width:100%;">
            <span style="font-size:14px;">Login</span>
        </a>
    </div>
</div>
</body>
</html>