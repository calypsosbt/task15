package calypso_team.task15.middleServer;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBInitTable {

    private DBHelper dbHelper;
    private Connection connection;

    public DBInitTable(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
        createAllTables();
    }

    private void createAllTables() {
        Statement st = null;
        try {
            st = dbHelper.getConnection().createStatement();
            dropAllTable(st);
            createTarif(st);
            createSubTarif(st);
            createTimeTarif(st);
            createConsumerInfo(st);
            createDevices(st);
            createMeters(st);
            createStatistic(st);
            st.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean checkTable(TableName tableName) {
        Statement st = null;
        try {
            st = dbHelper.getConnection().createStatement();
            st.execute("SELECT NULL FROM " + tableName.toString());
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public void createTarif(Statement statement) throws SQLException {
        if (statement == null || checkTable(TableName.TARIF)) {
            return;
        }
        statement.execute("CREATE TABLE " + TableName.TARIF + " ( " +
               " TARIFF_ID NUMBER PRIMARY KEY, " +
               " TARIFF_NAME VARCHAR(50) NOT NULL " +
               "); ");
    }

    public void createSubTarif(Statement statement) throws SQLException {
        if (statement == null || checkTable(TableName.SUB_TARIF)) {
            return;
        }
        statement.execute("CREATE TABLE " + TableName.SUB_TARIF + " ( " +
               " SUB_TARIFF_ID NUMBER PRIMARY KEY, " +
               " TARIFF_ID NUMBER NOT NULL, " +
               " DATE_BEGIN DATE NOT NULL, " +
               " DATE_END DATE, " +
               " PRICE NUMBER NOT NULL, " +
               " FOREIGN KEY (TARIFF_ID) REFERENCES " + TableName.TARIF + "(TARIFF_ID) " +
               "); ");
    }

    public void createTimeTarif(Statement statement) throws SQLException {
        if (statement == null || checkTable(TableName.TIME_TARIF)) {
            return;
        }
        statement.execute("CREATE TABLE " + TableName.TIME_TARIF + "( " +
               " SUB_TARIFF_ID NUMBER NOT NULL, " +
               " START_HOUR NUMBER NOT NULL, " +
               " END_HOUR NUMBER NOT NULL, " +
               " FOREIGN KEY (SUB_TARIFF_ID) REFERENCES " + TableName.SUB_TARIF + "(SUB_TARIFF_ID) " +
               "); ");
    }

    private void createConsumerInfo(Statement statement) throws SQLException {
        if (statement == null || checkTable(TableName.CONSUMER_INFO)) {
            return;
        }
        statement.execute("CREATE TABLE " + TableName.CONSUMER_INFO + "( " +
               " CONSUMER_ID NUMBER PRIMARY KEY, " +
               " LEGAL_ID NUMBER, " +
               " FIRST_NAME VARCHAR(256) NOT NULL, " +
               " SURNAME VARCHAR(256) NOT NULL, " +
               " THIRD_NAME VARCHAR(256), " +
               " LEGAL_NAME VARCHAR(256), " +
               " LOGIN_BIG_SERV VARCHAR(64), " +
               " LOGIN_ADMIN VARCHAR(64), " +
               " PASS_ADMIN VARCHAR(64), " +
               " GUID VARCHAR(512) NOT NULL " +
               "); ");
        dbHelper.inputConsumer(new ConsumerInfo(0,0,"Your name","Your Surname","Your ThirdName","NONE","NONE","admin","admin","NONE"));
    }


    private void createDevices(Statement statement) throws SQLException {
        if (statement == null || checkTable(TableName.DEVICES)) {
            return;
        }
        statement.execute("CREATE TABLE " + TableName.DEVICES + "( " +
               " DEVICE_ID NUMBER auto_increment PRIMARY KEY, " +
               " IDENTIFICATOR VARCHAR(256) NOT NULL, " +
               " DEVICE_NAME VARCHAR(256), " +
               " MAKE_MOVE NUMBER, " +
               " POWER NUMBER " +
               "); ");
        statement.execute("INSERT INTO " + TableName.DEVICES + " (IDENTIFICATOR, DEVICE_NAME, MAKE_MOVE, POWER) VALUES ('hashID','Плита',1,123);\n");
        statement.execute("INSERT INTO " + TableName.DEVICES + " (IDENTIFICATOR, DEVICE_NAME, MAKE_MOVE, POWER) VALUES ('hashID','Стиралка',0,234);\n");
        statement.execute("INSERT INTO " + TableName.DEVICES + " (DEVICE_ID, IDENTIFICATOR, DEVICE_NAME, MAKE_MOVE, POWER) VALUES (12345, 'hashID','TV',0,234);\n");

    }


    private void dropAllTable(Statement statement) throws SQLException {
        for (TableName tbaName : TableName.values()) {
            if (statement == null || !checkTable(tbaName)) {
                return;
            }
            statement.execute("DROP TABLE " + tbaName);
        }
    }

    private void createStatistic(Statement statement) throws SQLException {
        if (statement == null || checkTable(TableName.STATISTIC)) {
            return;
        }
        statement.execute("CREATE TABLE " + TableName.STATISTIC + "( " +
               " DEVICE_ID NUMBER NOT NULL, " +
               " DAY DATE NOT NULL, " +
               " HOUR NUMBER NOT NULL, " +
               " MINUTE NUMBER NOT NULL, " +
               " AMPER NUMBER NOT NULL, " +
               " VOLTAG NUMBER NOT NULL, " +
               " POWER NUMBER NOT NULL, " +
               " FOREIGN KEY (DEVICE_ID) REFERENCES " + TableName.DEVICES + "(DEVICE_ID) " +
               ");");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (1, '2018-11-01', 20, 6, 2, 3, 12);\n");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (1, '2018-11-01', 19, 6, 2, 3, 12);\n");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (1, '2018-11-01', 19, 6, 2, 3, 12);\n");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (1, '2018-11-01', 20, 6, 2, 3, 12);\n");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (2, '2018-11-01', 20, 6, 2, 3, 12);\n");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (2, '2018-11-01', 18, 6, 2, 3, 12);\n");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (2, '2018-11-01', 18, 6, 2, 3, 12);\n");
        statement.execute("INSERT INTO " + TableName.STATISTIC + " (DEVICE_ID, DAY, HOUR, MINUTE, AMPER, VOLTAG, POWER) VALUES (2, '2018-11-01', 20, 6, 2, 3, 12);\n");

    }

    private void createMeters(Statement statement) throws SQLException {
        if (statement == null || checkTable(TableName.METERS)) {
            return;
        }
        statement.execute("CREATE TABLE " + TableName.METERS + "( " +
               " LEGAL_ID NUMBER, " +
               " DATE_OF DATE, " +
               " POWER_OF NUMBER, " +
               " COST_OF NUMBER, " +
               " FOREIGN KEY (LEGAL_ID) REFERENCES " + TableName.CONSUMER_INFO + "(LEGAL_ID) " +
               ");");

    }
}
