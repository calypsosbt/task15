package calypso_team.task15.middleServer;

import calypso_team.task15.model.Statistic;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static calypso_team.task15.middleServer.ConsumerInfo.ColumnsConsumer.*;

public class DBHelper extends HttpServlet {

    private Connection connection;
    private static DBHelper dbHelper;

    private DBHelper() {
    }

    public static DBHelper newInstance(ServletContext servletContext) throws SQLException {
        if (dbHelper == null) {
            String dbName = servletContext.getInitParameter("dbName");
            String dbPass = servletContext.getInitParameter("dbPass");
            String dbUser = servletContext.getInitParameter("dbUser");
            try {
                Class.forName("org.h2.Driver").newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            Connection conn = DriverManager.getConnection("jdbc:h2:./" + dbName, dbUser, dbPass);
            dbHelper = new DBHelper();
            dbHelper.setConnection(conn);
            new DBInitTable(dbHelper);
        }
        return dbHelper;
    }

    public Connection getConnection() {
        return connection;
    }

    private Connection setConnection(Connection connection) {
        return this.connection = connection;
    }

    public boolean inputStatistic(Statistics statistic) {
        Statement st = null;
        try {
            st = connection.createStatement();
            PreparedStatement prSt = connection.prepareStatement("INSERT INTO " + TableName.STATISTIC + " VALUES ( ?, ?, ?, ?, ?, ?, ? )");
            prSt.setInt(1, statistic.getDeviceId());
            prSt.setString(2, statistic.getDate());
            prSt.setInt(3, statistic.getHour());
            prSt.setInt(4, statistic.getMinutes());
            prSt.setInt(5, statistic.getAmper());
            prSt.setInt(6, statistic.getVolt());
            prSt.setInt(7, statistic.getPower());
            prSt.execute();
            prSt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeStatement(st);
        }
    }

    public boolean inputConsumer(ConsumerInfo consumerInfo) {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM " + TableName.CONSUMER_INFO);
            statement.close();
            PreparedStatement prSt = connection.prepareStatement("INSERT INTO " + TableName.CONSUMER_INFO + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
            prSt.setInt(1, consumerInfo.getConsumerId());
            prSt.setInt(2, consumerInfo.getLegalId());
            prSt.setString(3, consumerInfo.getFirstName());
            prSt.setString(4, consumerInfo.getSurName());
            prSt.setString(5, consumerInfo.getThirdName());
            prSt.setString(6, consumerInfo.getLegalName());
            prSt.setString(7, consumerInfo.getLoginBigServ());
            prSt.setString(8, consumerInfo.getLoginAdmin());
            prSt.setString(9, consumerInfo.getPassAdmin());
            prSt.setString(10, consumerInfo.getGuid());
            prSt.execute();
            prSt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    public ResultSet selectConsumer() {
        try {
            Statement statement = connection.createStatement();
            return statement.executeQuery("SELECT * from " + TableName.CONSUMER_INFO);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ConsumerInfo getConsumerInfo() {
        ResultSet rs = selectConsumer();
        ConsumerInfo consumerInfo = new ConsumerInfo();
        try {
            while (rs.next()) {
                consumerInfo.setConsumerId(rs.getInt(CONSUMER_ID.name()));
                consumerInfo.setFirstName(rs.getString(FIRST_NAME.name()));
                consumerInfo.setSurName(rs.getString(SURNAME.name()));
                consumerInfo.setThirdName(rs.getString(THIRD_NAME.name()));
                consumerInfo.setGuid(rs.getString(GUID.name()));
                consumerInfo.setLegalId(rs.getInt(LEGAL_ID.name()));
                consumerInfo.setLegalName(rs.getString(LEGAL_NAME.name()));
                consumerInfo.setLoginBigServ(rs.getString(LOGIN_BIG_SERV.name()));
                consumerInfo.setLoginAdmin(rs.getString(LOGIN_ADMIN.name()));
                consumerInfo.setPassAdmin(rs.getString(PASS_ADMIN.name()));
            }
        } catch (SQLException e) {
            return null;
        } finally {
            closeResultSet(rs);
        }
        return consumerInfo;
    }

    public List<Statistic> getGroupStatistic(java.util.Date date, int hour, int legalId) {
        Statement statement = null;
        ResultSet resultSet = null;
        List<Statistic> listStatistic = new ArrayList<>();

        try {
            statement = connection.createStatement();
            StringBuilder query = new StringBuilder();
            query.append("select s.DEVICE_ID, s.DAY, s.HOUR, sum(s.POWER) ")
                    .append(" from STATISTIC s ")
                    .append(buildWhere(date, hour))
                    .append(" group by DEVICE_ID, DAY, HOUR");
            resultSet = statement.executeQuery(query.toString());
            while (resultSet.next()) {
                Statistic statistic = new Statistic();
                statistic.setDeviceId(resultSet.getLong(1));
                statistic.setDay(resultSet.getDate(2));
                statistic.setHour(resultSet.getLong(3));
                statistic.setPower(resultSet.getLong(4));
                statistic.setLegalId(legalId);
                listStatistic.add(statistic);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
            closeResultSet(resultSet);
        }
        return listStatistic;
    }

    public void deleteStatistic(java.util.Date date, int hour) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            StringBuilder query = new StringBuilder();
            query.append("delete from STATISTIC ")
                    .append(buildWhere(date, hour));
            statement.executeUpdate(query.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
        }
    }

    private void closeResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private String buildWhere(java.util.Date date, int hour) {
        StringBuilder where = new StringBuilder();
        where.append("where day <= '")
                .append(convertDateToString("yyyy-MM-dd", date))
                .append("' /*and hour < ").append(hour).append("*/");
        return where.toString();
    }

    public static synchronized String convertDateToString(String format, java.util.Date date) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

}
