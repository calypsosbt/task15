package calypso_team.task15.middleServer;

import java.sql.Date;

public class Statistics {
    private int deviceId;
    private String date;
    private int hour;
    private int minutes;
    private int amper;
    private int volt;
    private int power;


    public Statistics(int deviceId, String date, int hour, int minutes, int amper, int volt) {
        this.deviceId = deviceId;
        this.date = date;
        this.hour = hour;
        this.minutes = minutes;
        this.amper = amper;
        this.volt = volt;
        this.power = amper * amper * volt;
    }

    public Statistics(){}

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getAmper() {
        return amper;
    }

    public void setAmper(int amper) {
        this.amper = amper;
    }

    public int getVolt() {
        return volt;
    }

    public void setVolt(int volt) {
        this.volt = volt;
    }

    public int getPower() {
        return this.power;
    }

    public void setPower(int power) {
        this.power = this.volt * this.amper;
    }
}

