package calypso_team.task15.middleServer;

public class ConsumerInfo {
    public String getLegalName() {
        return legalName;
    }

    private int consumerId;
    private int legalId;
    private String firstName;
    private String surName;
    private String thirdName;
    private String legalName;
    private String loginBigServ;
    private String loginAdmin;
    private String passAdmin;
    private String guid;

    public enum ColumnsConsumer {
        CONSUMER_ID,
        LEGAL_ID,
        FIRST_NAME,
        SURNAME,
        THIRD_NAME,
        LEGAL_NAME,
        LOGIN_BIG_SERV,
        LOGIN_ADMIN,
        PASS_ADMIN,
        GUID
    }

    public ConsumerInfo() {
    }

    public ConsumerInfo(int consumerId, int legalId, String firstName, String surName, String thirdName, String legalName, String loginBigServ, String loginAdmin, String passAdmin, String guid) {
        this.consumerId = consumerId;
        this.legalId = legalId;
        this.firstName = firstName;
        this.surName = surName;
        this.thirdName = thirdName;
        this.legalName = legalName;
        this.loginBigServ = loginBigServ;
        this.loginAdmin = loginAdmin;
        this.passAdmin = passAdmin;
        this.guid = guid;
    }

    public int getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(int consumerId) {
        this.consumerId = consumerId;
    }

    public int getLegalId() {
        return legalId;
    }

    public void setLegalId(int legalId) {
        this.legalId = legalId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getThirdName() {
        return thirdName;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getLoginBigServ() {
        return loginBigServ;
    }

    public void setLoginBigServ(String loginBigServ) {
        this.loginBigServ = loginBigServ;
    }

    public String getLoginAdmin() {
        return loginAdmin;
    }

    public void setLoginAdmin(String loginAdmin) {
        this.loginAdmin = loginAdmin;
    }

    public String getPassAdmin() {
        return passAdmin;
    }

    public void setPassAdmin(String passAdmin) {
        this.passAdmin = passAdmin;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}
