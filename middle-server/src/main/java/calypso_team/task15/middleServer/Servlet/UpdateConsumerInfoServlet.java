package calypso_team.task15.middleServer.Servlet;

import calypso_team.task15.middleServer.ConsumerInfo;
import calypso_team.task15.middleServer.DBHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UpdateConsumerInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getParameter("username");
        String strNewUserConfig = request.getParameter("newUserConfig");
        JSONParser parser = new JSONParser();
        try {
            JSONArray jsonArray = (JSONArray)parser.parse(strNewUserConfig);
            if(jsonArray.size()!=1){return;}
            JSONObject jsObject = (JSONObject) jsonArray.get(0);
            int consumerId = Integer.valueOf(jsObject.get(ConsumerInfo.ColumnsConsumer.CONSUMER_ID.name()).toString());
            int legalId = Integer.valueOf(jsObject.get(ConsumerInfo.ColumnsConsumer.LEGAL_ID.name()).toString());
            String firstName = (String) jsObject.get(ConsumerInfo.ColumnsConsumer.FIRST_NAME.name());
            String surName = (String) jsObject.get(ConsumerInfo.ColumnsConsumer.SURNAME.name());
            String thirdName = (String) jsObject.get(ConsumerInfo.ColumnsConsumer.THIRD_NAME.name());
            String legalName = (String) jsObject.get(ConsumerInfo.ColumnsConsumer.LEGAL_NAME.name());
            String loginBigServ = (String) jsObject.get(ConsumerInfo.ColumnsConsumer.LOGIN_BIG_SERV.name());
            String loginAdmin = "admin";
            String passAdmin = "admin";
            String guid = (String) jsObject.get(ConsumerInfo.ColumnsConsumer.GUID.name());

            ConsumerInfo consumerInfo = new ConsumerInfo(consumerId,legalId,firstName,surName,thirdName,legalName,loginBigServ,loginAdmin,passAdmin,guid);
            DBHelper dbHelper = DBHelper.newInstance(getServletContext());
            dbHelper.inputConsumer(consumerInfo);
            response.setStatus(200);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


}
