package calypso_team.task15.middleServer.Servlet;

import calypso_team.task15.middleServer.ConsumerInfo;
import calypso_team.task15.middleServer.DBHelper;
import calypso_team.task15.model.Statistic;
import calypso_team.task15.rest.StatisticService;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ConsumerInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            DBHelper dbHelper = DBHelper.newInstance(getServletContext());
            ResultSet resultSet = dbHelper.selectConsumer();
            JSONArray rows = new JSONArray();
            while (resultSet.next()){
                JSONObject jsonObject = new JSONObject();
                for (ConsumerInfo.ColumnsConsumer column:ConsumerInfo.ColumnsConsumer.values()) {
                    jsonObject.put(column.name(), resultSet.getObject(column.name()));
                }
                rows.add(jsonObject);
            }

            response.setContentType("application/json");
            response.getWriter().write(rows.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
