package calypso_team.task15.middleServer.Servlet;

import calypso_team.task15.middleServer.DBHelper;
import calypso_team.task15.middleServer.Statistics;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;

public class ImportStatisticServlet  extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            try {
                DBHelper dbHelper = DBHelper.newInstance(getServletContext());
                Statistics statistics = buildStatistics(request);
                dbHelper.inputStatistic(statistics);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Statistics buildStatistics(HttpServletRequest request) {
        Calendar cal = Calendar.getInstance();
        String amper = request.getParameter("A");
        String volt = request.getParameter("V");
        String deviceId = request.getParameter("D");
        Statistics statistics = new Statistics();
        statistics.setMinutes(cal.get(Calendar.MINUTE));
        statistics.setHour(cal.get(Calendar.HOUR_OF_DAY));
        statistics.setDate(DBHelper.convertDateToString("yyyy-MM-dd", cal.getTime()));
        statistics.setAmper(Integer.valueOf(amper));
        statistics.setVolt(Integer.valueOf(volt));
        statistics.setPower(Integer.valueOf(amper) * Integer.valueOf(volt));
        statistics.setDeviceId(Integer.valueOf(deviceId));
        return statistics;
    }
}
