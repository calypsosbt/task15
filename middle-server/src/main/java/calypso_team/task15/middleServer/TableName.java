package calypso_team.task15.middleServer;

public enum TableName {
    TARIF,
    SUB_TARIF,
    TIME_TARIF,
    CONSUMER_INFO,
    DEVICES,
    METERS,
    STATISTIC,
    ;
}
