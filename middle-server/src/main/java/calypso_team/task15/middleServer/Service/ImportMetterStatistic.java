package calypso_team.task15.middleServer.Service;

import calypso_team.task15.middleServer.DBHelper;
import calypso_team.task15.middleServer.Statistics;

import javax.servlet.ServletContextEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ImportMetterStatistic implements Runnable {

    ServletContextEvent event ;

    public ImportMetterStatistic(ServletContextEvent event) {
        this.event = event;
    }

    @Override
    public void run() {
        String path = "C:\\files";
        File file = new File(path);
        listFilesForFolder(file);
    }

    private boolean saveFile(Statistics statistics) {
        try {
            return DBHelper.newInstance(event.getServletContext()).inputStatistic(statistics);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    private Statistics parseLine(String cvsLine) {
        Statistics statistics = new Statistics();
        statistics.setAmper(1);
        statistics.setDate("01/01.2019");
        statistics.setDeviceId(1);
        statistics.setHour(4);
        statistics.setMinutes(13);
        statistics.setPower(13);
        statistics.setVolt(156);
        return statistics;
    }

    private void parseCVS(final File file) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
            while (scanner.hasNext()) {
                Statistics statistics =   parseLine(scanner.nextLine());
                if (saveFile(statistics)) {
                    file.deleteOnExit();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }

    public void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isFile()) {
                parseCVS(fileEntry);
            }
        }
    }


}
