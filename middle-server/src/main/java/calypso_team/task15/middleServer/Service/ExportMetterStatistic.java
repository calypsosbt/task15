package calypso_team.task15.middleServer.Service;

import calypso_team.task15.middleServer.ConsumerInfo;
import calypso_team.task15.middleServer.DBHelper;
import calypso_team.task15.model.Statistic;
import calypso_team.task15.rest.StatisticService;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.servlet.ServletContextEvent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ExportMetterStatistic implements Runnable {
    ServletContextEvent event;

    public ExportMetterStatistic(ServletContextEvent event) {
        this.event = event;
    }


    @Override
    public void run() {
        Calendar cal = Calendar.getInstance();
        ConsumerInfo consumerInfo = null;
        try {
            consumerInfo = DBHelper.newInstance(event.getServletContext()).getConsumerInfo();
            List<Statistic> items = DBHelper.newInstance(event.getServletContext()).getGroupStatistic(getDate(cal), getHour(cal), consumerInfo.getLegalId());
            sendPost(items);
            DBHelper.newInstance(event.getServletContext()).deleteStatistic(getDate(cal), getHour(cal));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getHour(Calendar calendar) {
        return calendar.get(Calendar.HOUR_OF_DAY) == 0 ? 24 : calendar.get(Calendar.HOUR_OF_DAY);
    }

    private Date getDate(Calendar calendar) {
        return calendar.getTime();
    }

    private void sendPost(List< Statistic > items) throws Exception {
            Client client = ResteasyClientBuilder.newClient();
            WebTarget target = client.target("http://130.193.56.123:8080/profile-management-0.0.1-SNAPSHOT/pm");
            ResteasyWebTarget rtarget = (ResteasyWebTarget)target;
            StatisticService service = rtarget.proxy(StatisticService.class);
            service.insert(items);
    }
}
