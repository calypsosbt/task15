package calypso_team.task15.middleServer.ScheduledExecutor;

import calypso_team.task15.middleServer.Service.ExportMetterStatistic;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorExportStatisticMetter implements ServletContextListener {
    private ScheduledExecutorService scheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        Runnable runnable = new ExportMetterStatistic(event);
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(runnable, 0, 15, TimeUnit.SECONDS);
    }
}
