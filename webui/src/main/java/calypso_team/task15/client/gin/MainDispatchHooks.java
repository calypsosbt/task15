package calypso_team.task15.client.gin;

import com.google.gwt.http.client.Response;
import com.gwtplatform.dispatch.rest.client.RestDispatchHooks;
import com.gwtplatform.dispatch.rest.shared.RestAction;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;

import javax.inject.Inject;
import javax.inject.Provider;

public class MainDispatchHooks implements RestDispatchHooks {
    private final PlaceRequest toLoginRequest;
    private Provider<PlaceManager> placeManagerProvider;

    @Inject
    MainDispatchHooks(Provider<PlaceManager> placeManagerProvider) {
        this.placeManagerProvider = placeManagerProvider;
        toLoginRequest = new PlaceRequest.Builder().nameToken(NameTokens.enter).with("code", "401").build();
    }
    @Override
    public void onExecute(RestAction<?> action) {

    }

    @Override
    public void onSuccess(RestAction<?> action, Response response, Object result) {

    }

    @Override
    public void onFailure(RestAction<?> action, Response response, Throwable caught) {
        if (response.getStatusCode() == 401) {
            placeManagerProvider.get().revealUnauthorizedPlace("unauth-token");
        }
    }
}
