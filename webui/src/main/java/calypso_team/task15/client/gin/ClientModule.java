package calypso_team.task15.client.gin;

import calypso_team.task15.client.AppModule;
import com.gwtplatform.dispatch.rest.client.RestApplicationPath;
import com.gwtplatform.dispatch.rest.client.gin.RestDispatchAsyncModule;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;
import com.gwtplatform.mvp.shared.proxy.RouteTokenFormatter;

public class ClientModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        RestDispatchAsyncModule.Builder dispatchBuilder =
                new RestDispatchAsyncModule.Builder();
        install(dispatchBuilder.dispatchHooks(MainDispatchHooks.class).build());

        bind(CurrentSession.class).asEagerSingleton();

        bindConstant().annotatedWith(RestApplicationPath.class).to("pm");

        install(new DefaultModule.Builder()
                .tokenFormatter(RouteTokenFormatter.class)
                .defaultPlace(NameTokens.main)
                .errorPlace(NameTokens.main)
                .unauthorizedPlace(NameTokens.main)
                .build());

        install(new AppModule());

//        bindConstant().annotatedWith(SecurityCookie.class).to("Authorization");
    }
}

