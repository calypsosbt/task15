package calypso_team.task15.client.gin;

public class NameTokens {
    public static final String main = "/main";
    public static final String enter = "/enter";
    public static final String entrance = "/entrance";
    public static final String register = "/register";
    public static final String consumer_devices = "/consumer/{id}/devices";
    public static final String consumer_info = "/consumer/info";
    public static final String payment = "/payment";
    public static final String proposals = "/proposals";
    public static final String statistics = "/statistics";

    public static String getEnter() {
        return enter;
    }

    public static String getMain() {
        return main;
    }

    public static String getEntrance() {
        return entrance;
    }

    public static String getRegister() {
        return register;
    }

    public static String getConsumer_devices() {
        return consumer_devices;
    }

    public static String getConsumer_info() {
        return consumer_info;
    }

    public static String getPayment() {
        return payment;
    }

    public static String getProposals() {
        return proposals;
    }

    public static String getStatistics() {
        return statistics;
    }
}
