package calypso_team.task15.client.gin;

import calypso_team.task15.model.Consumer;
import com.google.gwt.user.client.Cookies;

public class CurrentSession {
    private boolean loggedIn;
    private Consumer consumer;

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    CurrentSession() {
        update();
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void update() {
        loggedIn = Cookies.getCookieNames().contains("Authorization");
    }
}
