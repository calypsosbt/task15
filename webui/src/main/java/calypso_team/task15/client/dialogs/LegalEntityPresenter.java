
package calypso_team.task15.client.dialogs;

import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;

public class LegalEntityPresenter extends PresenterWidget<LegalEntityPresenter.MyView> {
    public interface MyView extends View {
        void center();

        void hide();

        void showRelativeTo(Widget target);
    }

    @Inject
    LegalEntityPresenter(EventBus eventBus, MyView view) {
        super(eventBus, view);

    }

}
