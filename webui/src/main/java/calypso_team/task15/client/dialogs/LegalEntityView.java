
package calypso_team.task15.client.dialogs;

import calypso_team.task15.model.LegalEntities;
import calypso_team.task15.rest.LEService;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.ViewImpl;
import gwt.material.design.addins.client.window.MaterialWindow;
import gwt.material.design.client.ui.MaterialTextBox;

public class LegalEntityView extends ViewImpl implements LegalEntityPresenter.MyView {
    private ResourceDelegate<LEService> resourceDelegate;

    interface Binder extends UiBinder<Widget, LegalEntityView> {
    }

    @UiField
    MaterialWindow popup;
    @UiField
    MaterialTextBox legalId;
    @UiField
    MaterialTextBox legalName;
    @UiField
    MaterialTextBox legalAddress;

    @Inject
    LegalEntityView(
            Binder binder,
            EventBus eventBus,
            ResourceDelegate<LEService> resourceDelegate
    ) {
        this.resourceDelegate = resourceDelegate;
        initWidget(binder.createAndBindUi(this));
        MaterialWindow.setOverlay(true);
    }

    @Override
    public void center() {

    }

    @Override
    public void hide() {
        popup.close();
    }

    @Override
    public void showRelativeTo(Widget target) {
        popup.open();
    }

    @UiHandler("createBtn")
    void onCreateBtnClick(ClickEvent event) {
        resourceDelegate.withCallback(new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onSuccess(Void aVoid) {

            }
        }).create(getLegalEntity());
    }

    LegalEntities getLegalEntity() {
        LegalEntities le = new LegalEntities();
        le.setLegalId(Long.parseLong(legalId.getText()));
        le.setAddress(legalAddress.getText());
        le.setLegalFullName(legalName.getText());
        return le;
    }
}
