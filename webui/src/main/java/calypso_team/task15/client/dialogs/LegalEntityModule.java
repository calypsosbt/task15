
package calypso_team.task15.client.dialogs;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class LegalEntityModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenterWidget(LegalEntityPresenter.class, LegalEntityPresenter.MyView.class, LegalEntityView.class);
    }
}
