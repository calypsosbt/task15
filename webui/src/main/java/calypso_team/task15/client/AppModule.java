package calypso_team.task15.client;

import calypso_team.task15.client.app.AppHomeModule;
import calypso_team.task15.client.dialogs.LegalEntityModule;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

/**
 * @author Vasilii Starcev on 28.07.17.
 */
public class AppModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        install(new LegalEntityModule());
        install(new AppHomeModule());

//        bindPresenter(
//                AppHomePresenter.class,
//                AppHomePresenter.MyView.class,
//                AppHomeView.class,
//                AppHomePresenter.MyProxy.class
//        );
    }
}
