package calypso_team.task15.client.datasources;

import calypso_team.task15.model.LegalEntities;
import calypso_team.task15.model.Page;
import calypso_team.task15.rest.LEService;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import gwt.material.design.client.data.DataSource;
import gwt.material.design.client.data.loader.LoadCallback;
import gwt.material.design.client.data.loader.LoadConfig;
import gwt.material.design.client.data.loader.LoadResult;

@Singleton
public class LEDataSource implements DataSource<LegalEntities> {
    private ResourceDelegate<LEService> resourceDelegate;

    @Inject
    public LEDataSource(ResourceDelegate<LEService> resourceDelegate) {
        this.resourceDelegate = resourceDelegate;
    }

    @Override
    public void load(LoadConfig<LegalEntities> loadConfig, LoadCallback<LegalEntities> loadCallback) {
        Page<LegalEntities> page = Page.empty();
        page.setCount(loadConfig.getLimit());
        page.setPage(loadConfig.getOffset() / loadConfig.getLimit() + 1);
        resourceDelegate.withCallback(new AsyncCallback<Page<LegalEntities>>() {
            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onSuccess(Page<LegalEntities> legalEntitiesPage) {
                loadCallback.onSuccess(new LoadResult<>(
                        legalEntitiesPage.getItems(),
                        loadConfig.getOffset(),
                        legalEntitiesPage.getTotal()
                ));
            }
        }).filter(page);
    }

    public ResourceDelegate<LEService> getResourceDelegate() {
        return resourceDelegate;
    }

    @Override
    public boolean useRemoteSort() {
        return false;
    }
}
