package calypso_team.task15.client.app.statika;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import javax.inject.Inject;


public class ProposalsView extends ViewWithUiHandlers<ProposalsUiHandlers> implements ProposalsPresenter.MyView {
    interface Binder extends UiBinder<Widget, ProposalsView> {
    }

    @UiField
    SimplePanel main;

    @Inject
    ProposalsView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(ProposalsPresenter.SLOT_PROPOSALS, main);
    }
}
