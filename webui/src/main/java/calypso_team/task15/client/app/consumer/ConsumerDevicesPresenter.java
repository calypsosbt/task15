package calypso_team.task15.client.app.consumer;

import calypso_team.task15.client.app.AppHomePresenter;
import calypso_team.task15.client.gin.NameTokens;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

public class ConsumerDevicesPresenter extends Presenter<ConsumerDevicesPresenter.MyView, ConsumerDevicesPresenter.MyProxy> implements ConsumerDevicesUiHandlers {
    interface MyView extends View, HasUiHandlers<ConsumerDevicesUiHandlers> {
    }

    @NameToken(NameTokens.consumer_devices)
    @ProxyCodeSplit
    interface MyProxy extends ProxyPlace<ConsumerDevicesPresenter> {
    }

    public static final NestedSlot SLOT_CONSUMERDEVICES = new NestedSlot();

    @Inject
    ConsumerDevicesPresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy) {
        super(eventBus, view, proxy, AppHomePresenter.SLOT_APPHOME);

        getView().setUiHandlers(this);
    }

}
