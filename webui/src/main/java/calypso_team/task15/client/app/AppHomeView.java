package calypso_team.task15.client.app;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialPanel;

import javax.inject.Inject;


public class AppHomeView extends ViewWithUiHandlers<AppHomeUiHandlers> implements AppHomePresenter.MyView {

    interface Binder extends UiBinder<Widget, AppHomeView> {
    }

    @UiField
    MaterialPanel main;
    @UiField
    MaterialContainer slot;

    @Inject
    AppHomeView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(AppHomePresenter.SLOT_APPHOME, slot);

    }

}
