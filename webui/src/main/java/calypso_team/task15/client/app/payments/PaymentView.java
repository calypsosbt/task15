package calypso_team.task15.client.app.payments;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import javax.inject.Inject;


public class PaymentView extends ViewWithUiHandlers<PaymentUiHandlers> implements PaymentPresenter.MyView {
    interface Binder extends UiBinder<Widget, PaymentView> {
    }

    @UiField
    SimplePanel main;

    @Inject
    PaymentView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(PaymentPresenter.SLOT_PAYMENT, main);
    }
}
