package calypso_team.task15.client.app.statika;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import javax.inject.Inject;


public class StatikaBrowserView extends ViewWithUiHandlers<StatikaBrowserUiHandlers> implements StatikaBrowserPresenter.MyView {
    interface Binder extends UiBinder<Widget, StatikaBrowserView> {
    }

    @UiField
    SimplePanel main;

    @Inject
    StatikaBrowserView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(StatikaBrowserPresenter.SLOT_STATIKABROWSER, main);
    }
}
