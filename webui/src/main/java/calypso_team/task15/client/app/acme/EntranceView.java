package calypso_team.task15.client.app.acme;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import javax.inject.Inject;


public class EntranceView extends ViewWithUiHandlers<EntranceUiHandlers> implements EntrancePresenter.MyView {
    interface Binder extends UiBinder<Widget, EntranceView> {
    }

    @UiField
    SimplePanel main;

    @Inject
    EntranceView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(EntrancePresenter.SLOT_ENTRANCE, main);
    }
}
