package calypso_team.task15.client.app.acme;

import calypso_team.task15.client.gin.NameTokens;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

public class EntrancePresenter extends Presenter<EntrancePresenter.MyView, EntrancePresenter.MyProxy> implements EntranceUiHandlers {
    interface MyView extends View, HasUiHandlers<EntranceUiHandlers> {
    }

    @NameToken(NameTokens.entrance)
    @ProxyCodeSplit
    interface MyProxy extends ProxyPlace<EntrancePresenter> {
    }

    public static final NestedSlot SLOT_ENTRANCE = new NestedSlot();

    @Inject
    EntrancePresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy) {
        super(eventBus, view, proxy, RevealType.Root);

        getView().setUiHandlers(this);
    }

}
