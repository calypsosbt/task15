package calypso_team.task15.client.app.statika;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class StatikaModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(StatikaBrowserPresenter.class, StatikaBrowserPresenter.MyView.class, StatikaBrowserView.class, StatikaBrowserPresenter.MyProxy.class);
        bindPresenter(ProposalsPresenter.class, ProposalsPresenter.MyView.class, ProposalsView.class, ProposalsPresenter.MyProxy.class);
    }
}
