package calypso_team.task15.client.app.acme;

import calypso_team.task15.client.gin.NameTokens;
import calypso_team.task15.model.Consumer;
import calypso_team.task15.rest.ConsumerCRUDService;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

public class RegisterPresenter extends Presenter<RegisterPresenter.MyView, RegisterPresenter.MyProxy> implements RegisterUiHandlers {
    private ResourceDelegate<ConsumerCRUDService> consumerCRUDServiceResourceDelegate;
    private PlaceManager placeManager;

    @Override
    public void register(Consumer cons) {
        consumerCRUDServiceResourceDelegate
                .withCallback(new AsyncCallback<Boolean>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        getView().setErrorMessage("");
                    }

                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        if (aBoolean) {
                            placeManager.revealUnauthorizedPlace("");
                        } else {
                            getView().setErrorMessage("");
                        }
                    }
                })
                .register(cons);
    }

    interface MyView extends View, HasUiHandlers<RegisterUiHandlers> {
        void setErrorMessage(String s);
    }

    @NameToken(NameTokens.register)
    @ProxyCodeSplit
    interface MyProxy extends ProxyPlace<RegisterPresenter> {
    }

    public static final NestedSlot SLOT_REGISTER = new NestedSlot();

    @Inject
    RegisterPresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy,
            ResourceDelegate<ConsumerCRUDService> consumerCRUDServiceResourceDelegate,
            PlaceManager placeManager
    ) {
        super(eventBus, view, proxy, RevealType.Root);
        this.consumerCRUDServiceResourceDelegate = consumerCRUDServiceResourceDelegate;
        this.placeManager = placeManager;

        getView().setUiHandlers(this);
    }

}
