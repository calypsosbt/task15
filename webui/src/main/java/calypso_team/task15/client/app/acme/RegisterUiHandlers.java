package calypso_team.task15.client.app.acme;

import calypso_team.task15.model.Consumer;
import com.gwtplatform.mvp.client.UiHandlers;

interface RegisterUiHandlers extends UiHandlers {
    void register(Consumer cons);
}
