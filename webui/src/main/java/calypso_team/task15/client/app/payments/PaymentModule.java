package calypso_team.task15.client.app.payments;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class PaymentModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(PaymentPresenter.class, PaymentPresenter.MyView.class, PaymentView.class, PaymentPresenter.MyProxy.class);
    }
}
