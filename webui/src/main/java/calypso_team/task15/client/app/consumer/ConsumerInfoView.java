package calypso_team.task15.client.app.consumer;

import calypso_team.task15.client.datasources.LEDataSource;
import calypso_team.task15.client.dialogs.LegalEntityPresenter;
import calypso_team.task15.client.gin.CurrentSession;
import calypso_team.task15.model.Consumer;
import calypso_team.task15.model.LegalEntities;
import calypso_team.task15.rest.ConsumerCRUDService;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialPanel;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.animate.MaterialAnimation;
import gwt.material.design.client.ui.animate.Transition;
import gwt.material.design.client.ui.pager.MaterialDataPager;
import gwt.material.design.client.ui.table.MaterialDataTable;
import gwt.material.design.client.ui.table.cell.TextColumn;

import javax.inject.Inject;


public class ConsumerInfoView extends ViewWithUiHandlers<ConsumerInfoUiHandlers> implements ConsumerInfoPresenter.MyView {
    private ResourceDelegate<ConsumerCRUDService> consumerCRUDServiceResourceDelegate;
    private LEDataSource leDs;
    private LegalEntityPresenter legalEntityPresenter;
    private CurrentSession session;

    interface Binder extends UiBinder<Widget, ConsumerInfoView> {
    }

    @UiField
    MaterialPanel main;
    @UiField
    MaterialDataTable<LegalEntities> table;
    @UiField
    MaterialButton updateInfo;
    @UiField
    MaterialTextBox consumerLogin;
    @UiField
    MaterialTextBox consumerFirstname;
    @UiField
    MaterialTextBox consumerSurename;
    @UiField
    MaterialTextBox consumerLastname;
    private MaterialDataPager<LegalEntities> pager;

    @Inject
    ConsumerInfoView(
            Binder uiBinder,
            ResourceDelegate<ConsumerCRUDService> consumerCRUDServiceResourceDelegate,
            LEDataSource leDs,
            LegalEntityPresenter legalEntityPresenter,
            CurrentSession session
    ) {
        this.consumerCRUDServiceResourceDelegate = consumerCRUDServiceResourceDelegate;
        this.leDs = leDs;
        this.legalEntityPresenter = legalEntityPresenter;
        this.session = session;
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(ConsumerInfoPresenter.SLOT_CONSUMERINFO, main);

        table.getTableTitle().setText("Договора");

        table.setVisibleRange(1, 10);

        pager = new MaterialDataPager(table, leDs);
        table.add(pager);

        table.addColumn(new TextColumn<LegalEntities>() {
            {setWidth("30");}
            @Override
            public String getValue(LegalEntities legalEntities) {
                return legalEntities.getLegalFullName();
            }
        }, "Название");
        table.addColumn(new TextColumn<LegalEntities>() {
            @Override
            public String getValue(LegalEntities legalEntities) {
                return legalEntities.getAddress();
            }
        }, "Адрес");

        MaterialLink addLegalBtn = new MaterialLink();
        addLegalBtn.setIconType(IconType.ADD_CIRCLE_OUTLINE);
        addLegalBtn.addClickHandler(clickEvent -> {
            ((LegalEntityPresenter.MyView) legalEntityPresenter.getView()).showRelativeTo(table);
//            leDs.getResourceDelegate().withCallback(new AsyncCallback<Void>() {
//                @Override
//                public void onFailure(Throwable throwable) {
//
//                }
//
//                @Override
//                public void onSuccess(Void aVoid) {
//
//                }
//            }).create();
        });
        table.getScaffolding().getToolPanel().clear();
        table.getScaffolding().getToolPanel().add(addLegalBtn);

    }

    @UiHandler("updateInfo")
    void onUpdateConsumerClick(ClickEvent event) {
        updateInfo.setEnabled(false);
        updateInfo.setIconType(IconType.UPDATE);

        MaterialAnimation animation = new MaterialAnimation(updateInfo.getIcon());
        animation.transition(Transition.SHOW_GRID);
        animation.setInfinite(true);
        animation.animate();

        consumerCRUDServiceResourceDelegate.withCallback(new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable throwable) {
                animation.stopAnimation();
                updateInfo.setEnabled(true);
            }

            @Override
            public void onSuccess(Void aVoid) {
                animation.stopAnimation();
                updateInfo.setEnabled(true);
            }
        }).update(session.getConsumer().getId(), getConsumer());
    }

    private Consumer getConsumer() {
        Consumer consumer = session.getConsumer();
        consumer.setFirstName(consumerFirstname.getText());
        consumer.setSurname(consumerSurename.getText());
        consumer.setThirdName(consumerLastname.getText());
        return consumer;
    }

}
