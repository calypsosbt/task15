package calypso_team.task15.client.app.acme;

import calypso_team.task15.model.Consumer;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.html.Paragraph;

import javax.inject.Inject;


public class RegisterView extends ViewWithUiHandlers<RegisterUiHandlers> implements RegisterPresenter.MyView {
    @Override
    public void setErrorMessage(String s) {
        errorMsg.setText(s);
    }

    interface Binder extends UiBinder<Widget, RegisterView> {
    }

    @UiField
    SimplePanel main;
    @UiField
    MaterialTextBox regLogin;
    @UiField
    MaterialTextBox regEmail;
    @UiField
    MaterialTextBox regPassword;
    @UiField
    MaterialTextBox regPasswordAgain;
    @UiField
    MaterialButton btnRegister;
    @UiField
    Paragraph errorMsg;

    @Inject
    RegisterView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(RegisterPresenter.SLOT_REGISTER, main);
    }

    @UiHandler("btnRegister")
    void onRegiserClick(ClickEvent event) {
        if (regPassword.getText().equals(regPasswordAgain.getText())) {
            errorMsg.setText("");
            btnRegister.setEnabled(false);
            Consumer cons = new Consumer();
            cons.setEmail(regEmail.getText());
            cons.setNickname(regLogin.getText());
            cons.setHash(regPassword.getText());
            getUiHandlers().register(cons);
        } else {
            errorMsg.setText("Пароли не совпадают");
            btnRegister.setEnabled(true);
        }
    }
}
