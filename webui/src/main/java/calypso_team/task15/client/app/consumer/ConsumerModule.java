package calypso_team.task15.client.app.consumer;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class ConsumerModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(ConsumerInfoPresenter.class, ConsumerInfoPresenter.MyView.class, ConsumerInfoView.class, ConsumerInfoPresenter.MyProxy.class);
        bindPresenter(ConsumerDevicesPresenter.class, ConsumerDevicesPresenter.MyView.class, ConsumerDevicesView.class, ConsumerDevicesPresenter.MyProxy.class);
    }
}
