package calypso_team.task15.client.app.acme;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class AcmeModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(EntrancePresenter.class, EntrancePresenter.MyView.class, EntranceView.class, EntrancePresenter.MyProxy.class);
        bindPresenter(RegisterPresenter.class, RegisterPresenter.MyView.class, RegisterView.class, RegisterPresenter.MyProxy.class);
    }
}
