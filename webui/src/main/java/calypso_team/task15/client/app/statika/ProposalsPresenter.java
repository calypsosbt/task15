package calypso_team.task15.client.app.statika;

import calypso_team.task15.client.app.AppHomePresenter;
import calypso_team.task15.client.gin.NameTokens;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

public class ProposalsPresenter extends Presenter<ProposalsPresenter.MyView, ProposalsPresenter.MyProxy> implements ProposalsUiHandlers {
    interface MyView extends View, HasUiHandlers<ProposalsUiHandlers> {
    }

    @NameToken(NameTokens.proposals)
    @ProxyCodeSplit
    interface MyProxy extends ProxyPlace<ProposalsPresenter> {
    }

    public static final NestedSlot SLOT_PROPOSALS = new NestedSlot();

    @Inject
    ProposalsPresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy) {
        super(eventBus, view, proxy, AppHomePresenter.SLOT_APPHOME);

        getView().setUiHandlers(this);
    }

}
