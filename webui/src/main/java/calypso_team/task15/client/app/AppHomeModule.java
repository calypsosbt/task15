package calypso_team.task15.client.app;

import calypso_team.task15.client.app.acme.AcmeModule;
import calypso_team.task15.client.app.consumer.ConsumerModule;
import calypso_team.task15.client.app.payments.PaymentModule;
import calypso_team.task15.client.app.statika.StatikaModule;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class AppHomeModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        install(new PaymentModule());
        install(new StatikaModule());
        install(new ConsumerModule());
        install(new AcmeModule());

        bindPresenter(AppHomePresenter.class, AppHomePresenter.MyView.class, AppHomeView.class, AppHomePresenter.MyProxy.class);
    }
}
