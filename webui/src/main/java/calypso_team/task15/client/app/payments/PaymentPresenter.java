package calypso_team.task15.client.app.payments;

import calypso_team.task15.client.app.AppHomePresenter;
import calypso_team.task15.client.gin.NameTokens;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

public class PaymentPresenter extends Presenter<PaymentPresenter.MyView, PaymentPresenter.MyProxy> implements PaymentUiHandlers {
    interface MyView extends View, HasUiHandlers<PaymentUiHandlers> {
    }

    @NameToken(NameTokens.payment)
    @ProxyCodeSplit
    interface MyProxy extends ProxyPlace<PaymentPresenter> {
    }

    public static final NestedSlot SLOT_PAYMENT = new NestedSlot();

    @Inject
    PaymentPresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy) {
        super(eventBus, view, proxy, AppHomePresenter.SLOT_APPHOME);

        getView().setUiHandlers(this);
    }

}
