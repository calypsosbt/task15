package calypso_team.task15.client.app;

import calypso_team.task15.client.gin.NameTokens;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

public class AppHomePresenter extends Presenter<AppHomePresenter.MyView, AppHomePresenter.MyProxy> implements AppHomeUiHandlers {
    interface MyView extends View, HasUiHandlers<AppHomeUiHandlers> {
    }

    @NameToken(NameTokens.main)
    @ProxyStandard
    interface MyProxy extends ProxyPlace<AppHomePresenter> {
    }

    public static final NestedSlot SLOT_APPHOME = new NestedSlot();

    @Inject
    AppHomePresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy
    ) {
        super(eventBus, view, proxy, RevealType.Root);

        getView().setUiHandlers(this);
    }

}
