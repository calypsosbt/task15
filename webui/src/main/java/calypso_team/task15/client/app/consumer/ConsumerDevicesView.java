package calypso_team.task15.client.app.consumer;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import javax.inject.Inject;


public class ConsumerDevicesView extends ViewWithUiHandlers<ConsumerDevicesUiHandlers> implements ConsumerDevicesPresenter.MyView {
    interface Binder extends UiBinder<Widget, ConsumerDevicesView> {
    }

    @UiField
    SimplePanel main;

    @Inject
    ConsumerDevicesView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));

        bindSlot(ConsumerDevicesPresenter.SLOT_CONSUMERDEVICES, main);
    }
}
