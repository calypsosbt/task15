CREATE TABLE tarif(
	tarif_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'id тарифного плана',
	tarif_name VARCHAR(50) NOT NULL COMMENT 'Название тарифного плана'
);
ALTER TABLE tarif COMMENT 'Таблица тарифных планов';