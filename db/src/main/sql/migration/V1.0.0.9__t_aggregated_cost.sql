CREATE TABLE aggregated_cost(
	legal_id BIGINT COMMENT 'ID договора',
	date_of date COMMENT 'Дата',
	aggr_cost BIGINT COMMENT 'Агрегированная стоимость ээ',
	FOREIGN KEY (legal_id) REFERENCES legal_entities(legal_id)
);
ALTER TABLE aggregated_cost COMMENT 'Агрегированная стоимость ээ в месяце';

