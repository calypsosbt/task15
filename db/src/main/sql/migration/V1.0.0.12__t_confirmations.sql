create table confirmations(
  consumer_id bigint not null comment 'Consumer id',
  guid varchar(256) not null comment 'Device id',
  creation_date timestamp default current_timestamp() comment 'Время создания',
  active boolean default false comment 'is active',
  primary key (consumer_id),
  foreign key (consumer_id) references consumer(consumer_id)
);
alter table confirmations comment 'Таблица Активации регстраций пользователей';