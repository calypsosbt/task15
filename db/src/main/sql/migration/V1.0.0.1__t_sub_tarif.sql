CREATE TABLE sub_tarif(
	sub_tarif_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID периода действия тарифа',
	tarif_id BIGINT NOT NULL COMMENT 'ID тарифного плана',
	date_begin date NOT NULL COMMENT 'Дата начала действия тарифа',
	date_end date COMMENT 'Дата окончания действия тарифа',
	price BIGINT NOT NULL COMMENT 'Цена тарифа',
	FOREIGN KEY (tarif_id) REFERENCES tarif(tarif_id)
);
ALTER TABLE sub_tarif COMMENT 'Таблица разбивки тарифов';