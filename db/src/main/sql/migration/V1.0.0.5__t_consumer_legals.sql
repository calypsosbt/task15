CREATE TABLE consumer_legals(
	consumer_id BIGINT NOT NULL COMMENT 'ID пользователя',
	legal_id BIGINT NOT NULL COMMENT 'ID договора пользователя',
	FOREIGN KEY (consumer_id) REFERENCES consumer(consumer_id),
	FOREIGN KEY (legal_id) REFERENCES legal_entities(legal_id)
);
ALTER TABLE consumer_legals COMMENT 'Таблица-связка договоров с пользователями';
CREATE INDEX consleg_cons_id ON consumer_legals(consumer_id);
CREATE INDEX consleg_legalid ON consumer_legals(legal_id);