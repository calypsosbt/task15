CREATE TABLE devices(
	device_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID прибора',
	device_name VARCHAR(256) COMMENT 'Имя прибора',
	make_move BIGINT COMMENT 'Возможность варьировать время включения',
	power BIGINT COMMENT 'Потребляемая мощность прибора'
);
ALTER TABLE devices COMMENT 'Таблица-справочник приборов';
