CREATE TABLE meters(
	legal_id BIGINT COMMENT 'ID договора',
	sub_tarif_id BIGINT COMMENT 'ID периода действия тарифа',
	date_of date COMMENT 'Дата передачи показаний со счетчика',
	power_of BIGINT COMMENT 'Переданные показания со счетчика',
	cost_of BIGINT COMMENT 'Рассчетная стоимость по под-тарифу',
	FOREIGN KEY (legal_id) REFERENCES legal_entities(legal_id),
	FOREIGN KEY (sub_tarif_id) REFERENCES sub_tarif(sub_tarif_id)
);
ALTER TABLE meters COMMENT 'Данные со счетчика, разбитые по тарифам';