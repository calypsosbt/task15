CREATE TABLE consumer_devices(
	consumer_id BIGINT NOT NULL COMMENT 'ID пользователя',
	device_id BIGINT NOT NULL COMMENT 'ID прибора',
	FOREIGN KEY (device_id) REFERENCES devices(device_id),
	FOREIGN KEY (consumer_id) REFERENCES consumer(consumer_id)
);
ALTER TABLE consumer_devices COMMENT 'Таблица-связка пользователей и их приборов';