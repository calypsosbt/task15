CREATE TABLE consumer(
	consumer_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID пользователя',
	login VARCHAR(64) NOT NULL COMMENT 'Логин',
	email VARCHAR(128) NOT NULL COMMENT 'Емейл',
	first_name VARCHAR(256) NOT NULL COMMENT 'Имя пользователя',
	surname VARCHAR(256) NOT NULL COMMENT 'Фамилия пользователя',
	third_name VARCHAR(256) COMMENT 'Отчество пользователя',
	salt VARCHAR(64) NOT NULL,
	hash VARBINARY(32) DEFAULT NULL
);
ALTER TABLE consumer COMMENT 'Таблица пользователей';
ALTER TABLE consumer AUTO_INCREMENT=10000000;
CREATE INDEX consum_login_index ON consumer(login);
