CREATE TABLE archive(
	legal_id BIGINT NOT NULL COMMENT 'Legal id',
	device_id BIGINT NOT NULL COMMENT 'Device id',
	day date NOT NULL COMMENT 'День месяца',
	hour BIGINT NOT NULL COMMENT 'Час потребления',
	archive BLOB,
	PRIMARY KEY (legal_id, device_id),
	FOREIGN KEY (legal_id) REFERENCES legal_entities(legal_id),
	FOREIGN KEY (device_id) REFERENCES devices(device_id)
);
ALTER TABLE archive COMMENT 'Таблица статистики c локального сервера. Архивная';
CREATE INDEX archive_device_id ON statistic(device_id);
CREATE INDEX archive_device_id_day ON statistic(device_id, day);
CREATE INDEX archive_device_id_day_hour ON statistic(device_id, day, hour);
CREATE INDEX archive_day ON statistic(day);
CREATE INDEX archive_day_hour ON statistic(day,hour);