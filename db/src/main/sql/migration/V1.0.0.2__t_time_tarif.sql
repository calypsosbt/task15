CREATE TABLE period_sub_tarif(
	sub_tarif_id BIGINT NOT NULL COMMENT 'ID периода действия тарифа',
	start_hour BIGINT NOT NULL COMMENT 'Час начала действия периода',
	end_hour BIGINT NOT NULL COMMENT 'Час окончания действия периода',
	FOREIGN KEY (sub_tarif_id) REFERENCES sub_tarif(sub_tarif_id)
);
ALTER TABLE period_sub_tarif COMMENT 'Таблица разбивки тарифов';