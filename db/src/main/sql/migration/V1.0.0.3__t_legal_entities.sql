CREATE TABLE legal_entities (
	legal_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID договора',
	tarif_id BIGINT NOT NULL COMMENT 'ID тарифа',
	legal_full_name VARCHAR(256) NOT NULL COMMENT 'Название договора',
	address TEXT NOT NULL COMMENT 'Адрес',
	FOREIGN KEY (tarif_id) REFERENCES tarif(tarif_id)
);
ALTER TABLE legal_entities COMMENT 'Таблица договоров';
ALTER TABLE legal_entities AUTO_INCREMENT=10000000;