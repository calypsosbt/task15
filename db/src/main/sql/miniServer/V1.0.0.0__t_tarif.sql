CREATE TABLE tarif(
  tarif_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID тарифа',
  tarif_name VARCHAR(64) NOT NULL COMMENT 'Название тарифа'
);
ALTER TABLE tarif COMMENT 'Таблица тарифов';