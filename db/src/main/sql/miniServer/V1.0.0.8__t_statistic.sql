CREATE TABLE statistic(
  device_id BIGINT NOT NULL COMMENT 'Device id',
  time timestamp NOT NULL COMMENT 'Время',
  power BIGINT NOT NULL COMMENT 'Потребляемая мощность',
  FOREIGN KEY (device_id) REFERENCES devices(device_id)
);
ALTER TABLE statistic COMMENT 'Таблица статистики';
CREATE INDEX stat_device_id ON statistic(device_id);
CREATE INDEX stat_device_id_day ON statistic(device_id, time);
CREATE INDEX stat_day_hour ON statistic(time);
