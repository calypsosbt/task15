CREATE TABLE meters(
	sub_tarif_id BIGINT NOT NULL COMMENT 'ID периода действия тарифа',
	date_of date NOT NULL COMMENT 'Дата передачи показаний со счетчика',
	power_of BIGINT NOT NULL COMMENT 'Переданные показания со счетчика',
	cost_of BIGINT NOT NULL COMMENT 'Рассчетная стоимость по под-тарифу',
	FOREIGN KEY (sub_tarif_id) REFERENCES sub_tarif(sub_tarif_id)
);
ALTER TABLE meters COMMENT 'Данные со счетчика, разбитые по тарифам';
CREATE INDEX meters_tarif_date ON meters(sub_tarif_id, date_of);
