CREATE TABLE time_tarif(
	sub_tarif_id BIGINT NOT NULL COMMENT 'ID тарифа',
	start_hour BIGINT NOT NULL COMMENT 'Час начала действия тарифа',
	end_hour BIGINT NOT NULL COMMENT 'Час окончания действия тарифа',
	FOREIGN KEY (sub_tarif_id) REFERENCES sub_tarif(sub_tarif_id)
);
ALTER TABLE time_tarif COMMENT 'Таблица разбивки тарифов';
