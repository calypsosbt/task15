CREATE TABLE sub_tarif(
  sub_tarif_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID подтарифа',
  tarif_id BIGINT NOT NULL COMMENT 'ID тарифа',
  date_begin date NOT NULL COMMENT 'Дата начала действия тарифа',
  date_end date DEFAULT str_to_date('01.01.3000','%d.%m.%y') COMMENT 'Дата окончания действия тарифа',
  price BIGINT NOT NULL COMMENT 'Цена тарифа',
  FOREIGN KEY (tarif_id) REFERENCES tarif(tarif_id)
);
ALTER TABLE sub_tarif COMMENT 'Таблица разбивки тарифов';
