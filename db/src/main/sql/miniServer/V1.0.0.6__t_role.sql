CREATE TABLE user_roles
(
  role_id  				BIGINT COMMENT 'Роль пользвателя. степень двойки (0,1,2,4,8...)',
  role_description    	VARCHAR(64) COMMENT 'Описание'
);
ALTER TABLE user_roles COMMENT 'Таблица-ролей пользвателей системы';