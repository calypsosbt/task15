CREATE TABLE if not exists t_users
(
  id     	BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID пользователя' auto_increment,
  role  		VARCHAR(256) NOT NULL COMMENT 'Роль пользвателя (сумма user_roles.role_id)',
  nickname    	VARCHAR(64) NOT NULL COMMENT 'Nick name',
  hash    		VARCHAR(256) NOT NULL COMMENT 'Хеш',
  salt        VARCHAR(256) NOT NULL COMMENT 'Соль',
  first_name  	VARCHAR(256) COMMENT 'Имя',
  surname     	VARCHAR(256) COMMENT 'Фамилия',
  third_name  	VARCHAR(256) COMMENT 'Отчество'
);
ALTER TABLE t_users COMMENT 'Таблица пользвателей системы умный дом';
