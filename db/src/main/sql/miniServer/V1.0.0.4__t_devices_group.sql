CREATE TABLE devices_group(
	group_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID группы',
	group_name BIGINT COMMENT 'Имя группы',
	group_power BIGINT COMMENT 'Предполагаемая потребляемая мощность группы'
);
ALTER TABLE devices_group COMMENT 'Таблица групп приборов (кухня/ванна/комната)';