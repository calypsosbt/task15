CREATE TABLE devices(
	device_id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT 'ID прибора',
	group_id BIGINT COMMENT 'ID группы',
	device_name VARCHAR(256) COMMENT 'Имя прибора',
	make_move BIGINT COMMENT 'Возможность варьировать время включения',
	power BIGINT COMMENT 'Потребляемая мощность прибора',
	FOREIGN KEY (group_id) REFERENCES devices_group(group_id)
);
ALTER TABLE devices COMMENT 'Таблица приборов';