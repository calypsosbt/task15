CREATE TABLE consumer(
	consumer_id BIGINT PRIMARY KEY COMMENT 'ID пользователя с внешнего сервера',
	legal_id BIGINT NOT NULL COMMENT 'ID договора пользователя',
	legal_full_name VARCHAR(256) NOT NULL COMMENT 'Назавание договора',
	tarif_id BIGINT NOT NULL COMMENT 'Текущий тарифный план',
	authorization_pass TEXT NOT NULL COMMENT 'Пароль авторизации на сервере',
	first_name VARCHAR(256) NOT NULL COMMENT 'Имя пользователя',
	surname VARCHAR(256) NOT NULL COMMENT 'Фамилия пользователя',
	third_name VARCHAR(256) COMMENT 'Отчество пользователя',
	address TEXT COMMENT 'Адрес регистрации'
);
ALTER TABLE consumer COMMENT 'Таблица зарегистрированного пользователя';
