package calypso_team.task15.db;

import org.flywaydb.core.Flyway;

import javax.sql.DataSource;

public class MainDatabase {
    private static final Flyway get(DataSource dataSource) {
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setLocations("migration");
        return flyway;
    }

    public static int migrate(DataSource ds) {
        return get(ds).migrate();
    }

    public static void clean(DataSource ds) {
        get(ds).clean();
    }
}
