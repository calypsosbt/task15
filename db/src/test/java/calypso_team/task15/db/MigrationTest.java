package calypso_team.task15.db;

import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import org.flywaydb.core.Flyway;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MigrationTest {
    @Test
    public void countOnMySQL() throws Exception {
        File target = new File("target", "db");
        if (!target.exists()) {
            if (!target.mkdirs()) {
                throw new Exception("cant create " + target.getAbsolutePath());
            }
        }
        DBConfigurationBuilder configBuilder = DBConfigurationBuilder.newBuilder();
        configBuilder.setPort(3306);
        configBuilder.setDataDir(target.getAbsolutePath());
        configBuilder.setDatabaseVersion("mariadb-10.2.11");
        DB db = DB.newEmbeddedDB(configBuilder.build());
        db.start();
        db.createDB("main");

        Flyway flyway = new Flyway();
        String url = "jdbc:mysql://localhost:3306/main?characterEncoding=utf-8&useUnicode=true&serverTimezone=CTT";
        flyway.setDataSource(url, "root", "");
        flyway.setLocations("migration");
        flyway.clean();
        flyway.migrate();

        try (
                Connection con = DriverManager.getConnection(url, "root", "");
                PreparedStatement ps = con.prepareStatement("show tables;");
                ResultSet rs = ps.executeQuery()
        ) {
            Assert.assertTrue(rs.next());
        }
    }
}
