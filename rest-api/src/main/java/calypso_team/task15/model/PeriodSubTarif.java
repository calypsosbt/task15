package calypso_team.task15.model;


public class PeriodSubTarif {

  private long subTarifId;
  private long startHour;
  private long endHour;


  public long getSubTarifId() {
    return subTarifId;
  }

  public void setSubTarifId(long subTarifId) {
    this.subTarifId = subTarifId;
  }


  public long getStartHour() {
    return startHour;
  }

  public void setStartHour(long startHour) {
    this.startHour = startHour;
  }


  public long getEndHour() {
    return endHour;
  }

  public void setEndHour(long endHour) {
    this.endHour = endHour;
  }

}
