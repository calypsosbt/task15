package calypso_team.task15.model;
import java.sql.Date;


public class Meters {

  private long legalId;
  private long subTarifId;
  private Date dateOf;
  private long powerOf;
  private long costOf;


  public long getLegalId() {
    return legalId;
  }

  public void setLegalId(long legalId) {
    this.legalId = legalId;
  }


  public long getSubTarifId() {
    return subTarifId;
  }

  public void setSubTarifId(long subTarifId) {
    this.subTarifId = subTarifId;
  }


  public Date getDateOf() {
    return dateOf;
  }

  public void setDateOf(Date dateOf) {
    this.dateOf = dateOf;
  }


  public long getPowerOf() {
    return powerOf;
  }

  public void setPowerOf(long powerOf) {
    this.powerOf = powerOf;
  }


  public long getCostOf() {
    return costOf;
  }

  public void setCostOf(long costOf) {
    this.costOf = costOf;
  }

}
