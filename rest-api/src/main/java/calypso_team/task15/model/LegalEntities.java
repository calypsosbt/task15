package calypso_team.task15.model;


public class LegalEntities {

  private long legalId;
  private long tarifId;
  private String legalFullName;
  private String address;


  public long getLegalId() {
    return legalId;
  }

  public void setLegalId(long legalId) {
    this.legalId = legalId;
  }


  public long getTarifId() {
    return tarifId;
  }

  public void setTarifId(long tarifId) {
    this.tarifId = tarifId;
  }


  public String getLegalFullName() {
    return legalFullName;
  }

  public void setLegalFullName(String legalFullName) {
    this.legalFullName = legalFullName;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

}
