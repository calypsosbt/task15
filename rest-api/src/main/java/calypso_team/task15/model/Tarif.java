package calypso_team.task15.model;


public class Tarif {

  private long tarifId;
  private String tarifName;

  public long getTarifId() {
    return tarifId;
  }

  public void setTarifId(long tarifId) {
    this.tarifId = tarifId;
  }


  public String getTarifName() {
    return tarifName;
  }

  public void setTarifName(String tarifName) {
    this.tarifName = tarifName;
  }

}
