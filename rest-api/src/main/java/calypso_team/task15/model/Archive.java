package calypso_team.task15.model;
import java.sql.Date;


public class Archive {

  private long legalId;
  private long deviceId;
  private Date day;
  private long hour;
  private String archive;


  public long getLegalId() {
    return legalId;
  }

  public void setLegalId(long legalId) {
    this.legalId = legalId;
  }


  public long getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(long deviceId) {
    this.deviceId = deviceId;
  }


  public Date getDay() {
    return day;
  }

  public void setDay(Date day) {
    this.day = day;
  }


  public long getHour() {
    return hour;
  }

  public void setHour(long hour) {
    this.hour = hour;
  }


  public String getArchive() {
    return archive;
  }

  public void setArchive(String archive) {
    this.archive = archive;
  }

}
