package calypso_team.task15.model;


public class ConsumerLegals {

  private long consumerId;
  private long legalId;


  public long getConsumerId() {
    return consumerId;
  }

  public void setConsumerId(long consumerId) {
    this.consumerId = consumerId;
  }


  public long getLegalId() {
    return legalId;
  }

  public void setLegalId(long legalId) {
    this.legalId = legalId;
  }

}
