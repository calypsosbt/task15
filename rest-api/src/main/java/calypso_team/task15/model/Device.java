package calypso_team.task15.model;


public class Device {

  private long deviceId;
  private String deviceName;
  private long makeMove;
  private long power;


  public long getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(long deviceId) {
    this.deviceId = deviceId;
  }


  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }


  public long getMakeMove() {
    return makeMove;
  }

  public void setMakeMove(long makeMove) {
    this.makeMove = makeMove;
  }


  public long getPower() {
    return power;
  }

  public void setPower(long power) {
    this.power = power;
  }

}
