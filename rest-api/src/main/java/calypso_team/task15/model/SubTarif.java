package calypso_team.task15.model;
import java.sql.Date;


public class SubTarif {

  private long subTarifId;
  private long tarifId;
  private Date dateBegin;
  private Date dateEnd;
  private long price;


  public long getSubTarifId() {
    return subTarifId;
  }

  public void setSubTarifId(long subTarifId) {
    this.subTarifId = subTarifId;
  }


  public long getTarifId() {
    return tarifId;
  }

  public void setTarifId(long tarifId) {
    this.tarifId = tarifId;
  }


  public Date getDateBegin() {
    return dateBegin;
  }

  public void setDateBegin(Date dateBegin) {
    this.dateBegin = dateBegin;
  }


  public Date getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(Date dateEnd) {
    this.dateEnd = dateEnd;
  }


  public long getPrice() {
    return price;
  }

  public void setPrice(long price) {
    this.price = price;
  }

}
