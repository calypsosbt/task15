package calypso_team.task15.model;

import java.util.List;

public class Page<T> {
    private int page = 1;
    private int count = 25;
    private int total;
    private List<T> items;

    public static <E> Page<E> empty() {
        return new Page<E>();
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public int getOffset() {
        int local = page;
        if (local < 1) {
            local = 1;
        }
        return (local - 1) * count;
    }
}
