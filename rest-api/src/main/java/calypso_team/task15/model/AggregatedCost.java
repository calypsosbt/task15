package calypso_team.task15.model;
import java.sql.Date;

public class AggregatedCost {

  private long legalId;
  private Date dateOf;
  private long aggrCost;


  public long getLegalId() {
    return legalId;
  }

  public void setLegalId(long legalId) {
    this.legalId = legalId;
  }


  public Date getDateOf() {
    return dateOf;
  }

  public void setDateOf(Date dateOf) {
    this.dateOf = dateOf;
  }


  public long getAggrCost() {
    return aggrCost;
  }

  public void setAggrCost(long aggrCost) {
    this.aggrCost = aggrCost;
  }

}
