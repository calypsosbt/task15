package calypso_team.task15.model;
import java.sql.Date;


public class Statistic {

  private long legalId;
  private long deviceId;
  private Date day;
  private long hour;
  private long power;


  public long getLegalId() {
    return legalId;
  }

  public void setLegalId(long legalId) {
    this.legalId = legalId;
  }


  public long getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(long deviceId) {
    this.deviceId = deviceId;
  }


  public Date getDay() {
    return day;
  }

  public void setDay(Date day) {
    this.day = day;
  }


  public long getHour() {
    return hour;
  }

  public void setHour(long hour) {
    this.hour = hour;
  }


  public long getPower() {
    return power;
  }

  public void setPower(long power) {
    this.power = power;
  }

}
