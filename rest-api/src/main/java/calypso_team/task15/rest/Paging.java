package calypso_team.task15.rest;

import calypso_team.task15.model.Page;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Produces("application/json; charset=utf-8")
@Consumes("application/json; charset=utf-8")
public interface Paging<T> {
    @POST
    @Path("filter")
    Page<T> filter(Page<T> page);
}
