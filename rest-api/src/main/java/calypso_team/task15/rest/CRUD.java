package calypso_team.task15.rest;

import javax.ws.rs.*;

@Produces("application/json; charset=utf-8")
@Consumes("application/json; charset=utf-8")
public interface CRUD<T> {
    @GET
    @Path("{id}")
    T get(@PathParam("id") Long id);

    @POST
    @Path("{id}")
    void update(@PathParam("id") Long id, T item);

    @PUT
    void create(T item);

    @DELETE
    @Path("{id}")
    void delete(@PathParam("id") Long id);

}
