package calypso_team.task15.rest;

import calypso_team.task15.model.LegalEntities;

import javax.ws.rs.Path;

@Path("legals")
public interface LEService extends CRUD<LegalEntities>, Paging<LegalEntities> {
}
