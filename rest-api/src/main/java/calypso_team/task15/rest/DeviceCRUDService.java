package calypso_team.task15.rest;

import calypso_team.task15.model.Device;

import javax.ws.rs.Path;

@Path("device")
public interface DeviceCRUDService extends CRUD<Device>, Paging<Device> {
}
