package calypso_team.task15.rest;

import calypso_team.task15.model.Statistic;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

@Path("statistic")
@Produces("application/json; charset=utf-8")
@Consumes("application/json; charset=utf-8")
public interface StatisticService {

    @PUT
    @PermitAll
    void insert(List<Statistic> items);
}
