package calypso_team.task15.rest;

import calypso_team.task15.model.ChangePassword;
import calypso_team.task15.model.Consumer;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("consumer")
@Produces("application/json; charset=utf-8")
@Consumes("application/json; charset=utf-8")
public interface ConsumerCRUDService extends CRUD<Consumer>, Paging<Consumer> {
    @POST @Path("password")
    boolean setUserPassword(ChangePassword data);

    @POST @Path("secret")
    boolean setSecret(ChangePassword data);

    @GET
    @Path("page/{id}")
    List<Consumer> page(@PathParam("id") Integer id);

    @POST
    @Path("register")
    @PermitAll
    boolean register(Consumer consumer);

    @GET
    @Path("register")
    @PermitAll
    Response activate(@QueryParam("con_id") Long id, @QueryParam("guid") String guid);
}
