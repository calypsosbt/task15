package calypso_team.task15.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@ApplicationScoped
public class ConfirmationsDAO {
    @Inject @XADS
    DbConnection dbConnection;

    public String create(Long consumerId) throws SQLException {
        try (PreparedStatement ps = dbConnection.get().prepareStatement(
                "insert into confirmations(consumer_id, guid) \n" +
                "values (?, ?);")
        ) {
            String guid = UUID.randomUUID().toString();
            ps.setLong(1, consumerId);
            ps.setString(2, guid);

            if (ps.executeUpdate() == 1) {
                return guid;
            }
        }
        throw new SQLException("not found");
    }
    public boolean markIfExists(Long id, String guid) throws SQLException {
        try (PreparedStatement ps = dbConnection.get().prepareStatement(
                "update confirmations set active = true \n" +
                        "where consumer_id = ? and guid = ?;")
        ) {
            ps.setLong(1, id);
            ps.setString(2, guid);

            return ps.executeUpdate() == 1;
        }
    }
    public boolean isActive(Long id) throws SQLException {
        try (PreparedStatement ps = dbConnection.get().prepareStatement(
                "select active from confirmations where consumer_id = ?;")
        ) {
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.getBoolean(1);
            }
        }
    }
}
