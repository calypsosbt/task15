package calypso_team.task15.dao;

import calypso_team.task15.model.Statistic;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.PreparedStatement;

@ApplicationScoped
public class StatisticDAO {
    @Inject
    @XADS
    DbConnection dbConnection;

    public void insert(Statistic item) throws Exception {
        try (PreparedStatement ps = dbConnection.get().prepareStatement(
                "insert into statistic(legal_id, device_id, day, hour, power) \n" +
                        "values (?, ?, ?, ?, ?);")
        ) {
            ps.setLong(1, item.getLegalId());
            ps.setLong(2, item.getDeviceId());
            ps.setDate(3, item.getDay());
            ps.setLong(4, item.getHour());
            ps.setLong(5, item.getPower());
            ps.executeQuery();
        }
    }
}
