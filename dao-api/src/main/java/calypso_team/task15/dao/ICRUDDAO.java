package calypso_team.task15.dao;

import calypso_team.task15.model.Page;

import java.sql.SQLException;

public interface ICRUDDAO<T> {
    void create(T item) throws Exception;

    boolean update(Long id, T item) throws Exception;

    Integer count() throws Exception;

    Page<T> getPage(int pageNumber, int size) throws Exception;

    T getById(Long id) throws Exception;

    boolean delete(Long id) throws SQLException;
}
