package calypso_team.task15.dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface DbConnection {
    Connection get() throws SQLException;
}
