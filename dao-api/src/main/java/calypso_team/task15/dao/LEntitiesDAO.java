package calypso_team.task15.dao;

import calypso_team.task15.model.LegalEntities;
import calypso_team.task15.model.Page;
import calypso_team.task15.rest.Paging;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@ApplicationScoped
public class LEntitiesDAO implements ICRUDDAO<LegalEntities>, Paging<LegalEntities> {
    @Inject @XADS
    DbConnection dbConnection;

    @Override
    public void create(LegalEntities item) throws Exception {
        try (PreparedStatement ps = dbConnection.get().prepareStatement(
                "insert into legal_entities(tariff_id, legal_full_name, address) \n" +
                        "values (?, ?, ?);", Statement.RETURN_GENERATED_KEYS)
        ) {
            ps.setLong(1, item.getTarifId());
            ps.setString(2, item.getLegalFullName());
            ps.setString(3, item.getAddress());

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    item.setLegalId(rs.getLong(1));
                }
            }
        }
    }

    @Override
    public boolean update(Long id, LegalEntities item) throws Exception {
        try (PreparedStatement ps = dbConnection.get().prepareStatement(
                "update legal_entities set tariff_id = ?, legal_full_name = ?, address = ? where legal_id = ?;"
        )) {
            ps.setLong(1, item.getTarifId());
            ps.setString(2, item.getLegalFullName());
            ps.setString(3, item.getAddress());
            ps.setLong(4, item.getLegalId());

            return ps.executeUpdate() == 1;
        }
    }

    @Override
    public Integer count() throws Exception {
        try (
                PreparedStatement ps = dbConnection.get().prepareStatement("select count(*) from legal_entities;");
                ResultSet rs = ps.executeQuery()
        ) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }

    @Override
    public Page<LegalEntities> getPage(int pageNumber, int size) throws Exception {
        Page<LegalEntities> page = Page.empty();
        page.setCount(size);
        page.setPage(pageNumber);
        page.setTotal(count());
        page.setItems(new ArrayList<>(size));
        try (PreparedStatement ps = dbConnection.get().prepareStatement(
                "select * from legal_entities order by legal_id limit ? offset ?;"
        )) {
            ps.setLong(1, size);
            ps.setLong(2, page.getOffset());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    page.getItems().add(rs2le(rs));
                }
            }
        }
        return page;
    }

    @Override
    public LegalEntities getById(Long id) throws Exception {
        try (PreparedStatement ps = dbConnection.get().prepareStatement("select * from legal_entities where legal_id = ?;")) {
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs2le(rs);
                }
            }
        }
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        try (PreparedStatement ps = dbConnection.get().prepareStatement("delete from legal_entities where legal_id = ?;")) {
            ps.setLong(1, id);
            return ps.execute();
        }
    }

    private LegalEntities rs2le(ResultSet rs) throws SQLException {
        LegalEntities le = new LegalEntities();
        le.setLegalId(rs.getLong("legal_id"));
        le.setTarifId(rs.getLong("tarif_id"));
        le.setLegalFullName(rs.getString("legal_full_name"));
        le.setAddress(rs.getString("address"));
        return le;
    }

    @Override
    public Page<LegalEntities> filter(Page<LegalEntities> page) {
        try {
            return getPage(page.getPage(), page.getCount());
        } catch (Exception e) {
            e.printStackTrace();
            return Page.empty();
        }
    }
}
