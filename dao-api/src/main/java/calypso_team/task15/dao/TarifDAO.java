package calypso_team.task15.dao;

import calypso_team.task15.model.Page;
import calypso_team.task15.model.Tarif;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@ApplicationScoped
public class TarifDAO implements ICRUDDAO<Tarif> {
    @Inject @XADS
    DbConnection dbProvider;


    @Override
    public void create(Tarif item) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement(
                "insert into tarif (tarif_name) values (?)",
                Statement.RETURN_GENERATED_KEYS)
        ) {
            pstmt.setString(1, item.getTarifName());
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    item.setTarifId(rs.getLong(1));
                }
            }
        }
    }

    @Override
    public boolean update(Long id, Tarif item) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement("update tarif set tarif_name = ? where tarif_id = ?;")) {
            pstmt.setString(1, item.getTarifName());
            pstmt.setLong(2, item.getTarifId());

            return pstmt.executeUpdate() == 1;
        }
    }

    @Override
    public Integer count() throws Exception {
        try (
                PreparedStatement pstmt = dbProvider.get().prepareStatement("select count(*) from tarif;");
                ResultSet rs = pstmt.executeQuery()
        ) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }

    @Override
    public Page<Tarif> getPage(int pageNumber, int size) throws Exception {
        return null;
    }

    @Override
    public Tarif getById(Long id) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement("select * from tarif where tarif_id = ?")) {
            pstmt.setLong(1, id);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    Tarif Tarif = new Tarif();
                    Tarif.setTarifName(rs.getString("tarif_name"));
                    return Tarif;
                }
            }
        }
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        return false;
    }
}
