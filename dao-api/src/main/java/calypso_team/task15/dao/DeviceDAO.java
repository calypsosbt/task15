package calypso_team.task15.dao;

import calypso_team.task15.model.Device;
import calypso_team.task15.model.Page;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@ApplicationScoped
public class DeviceDAO implements ICRUDDAO<Device> {
    @Inject @XADS
    DbConnection dbProvider;


    @Override
    public void create(Device item) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement(
                "insert into devices (device_name, make_move, power) values (?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)
        ) {
            int idx = 1;
            pstmt.setString(idx++, item.getDeviceName());
            pstmt.setLong(idx++, item.getMakeMove());
            pstmt.setLong(idx, item.getPower());

            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    item.setDeviceId(rs.getLong(1));
                }
            }
        }
    }

    @Override
    public boolean update(Long id, Device item) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement("update devices set device_name = ?, power = ?, make_move = ? where device_id = ?;")) {
            pstmt.setString(1, item.getDeviceName());
            pstmt.setLong(2, item.getPower());
            pstmt.setLong(3, item.getMakeMove());
            pstmt.setLong(4, item.getDeviceId());

            return pstmt.executeUpdate() == 1;
        }
    }

    @Override
    public Integer count() throws Exception {
        try (
                PreparedStatement pstmt = dbProvider.get().prepareStatement("select count(*) from devices;");
                ResultSet rs = pstmt.executeQuery()
        ) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }

    @Override
    public Page<Device> getPage(int pageNumber, int size) throws Exception {
        return null;
    }

    @Override
    public Device getById(Long id) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement("select * from devices where device_id = ?")) {
            pstmt.setLong(1, id);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    Device device = new Device();
                    device.setDeviceName(rs.getString("device_name"));
                    device.setPower(rs.getLong("power"));
                    device.setMakeMove(rs.getLong("make_move"));
                    return device;
                }
            }
        }
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        return false;
    }
}
