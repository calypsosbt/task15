package calypso_team.task15.dao;

import calypso_team.task15.model.Consumer;
import calypso_team.task15.model.Page;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

@ApplicationScoped
public class ConsumerDAO implements ICRUDDAO<Consumer> {
    @Inject @XADS
    DbConnection dbProvider;

    @Override
    public void create(Consumer consumer) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement(
                "insert into consumer(login, email, first_name, surname, third_name, salt) values (?, ?, ?, ?, ?, ?);",
                Statement.RETURN_GENERATED_KEYS)
        ) {
            consumer.setSalt(generateString(new Random()));
            int idx = 1;
            pstmt.setString(idx++, consumer.getNickname());
            pstmt.setString(idx++, consumer.getEmail());
            if (consumer.getFirstName() == null) {
                pstmt.setString(idx++, "");
            } else {
                pstmt.setString(idx++, consumer.getFirstName());
            }
            if (consumer.getSurname() == null) {
                pstmt.setString(idx++, "");
            } else {
                pstmt.setString(idx++, consumer.getSurname());
            }
            if (consumer.getThirdName() == null) {
                pstmt.setNull(idx++, Types.VARCHAR);
            } else {
                pstmt.setString(idx++, consumer.getThirdName());
            }
            pstmt.setString(idx, consumer.getSalt());
            pstmt.execute();

            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                if (rs.next()) {
                    consumer.setId(rs.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new Exception("Create consumer exception", e);
        }
    }

    @Override
    public boolean update(Long id, Consumer consumer) throws Exception {
        String sql = "update consumer set first_name = ?, surname = ?, third_name = ?, email = ? where consumer_id = ?";
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement(sql)) {
            int idx = 1;
            pstmt.setString(idx++, consumer.getFirstName());
            pstmt.setString(idx++, consumer.getSurname());
            pstmt.setString(idx++, consumer.getThirdName());
            pstmt.setString(idx++, consumer.getEmail());
            pstmt.setLong(idx, id);

            return pstmt.executeUpdate() == 1;
        }
    }

    @Override
    public Integer count() throws Exception {
        try (
                PreparedStatement pstmt = dbProvider.get().prepareStatement("select count(*) from consumer;");
                ResultSet rs = pstmt.executeQuery()
        ) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            throw new Exception("uncountable", e);
        }
        throw new Exception("uncountable");
    }

    @Override
    public Page<Consumer> getPage(int pageNumber, int size) throws Exception {
        Page<Consumer> page = new Page<>();
        page.setCount(size);
        page.setPage(pageNumber);
        page.setTotal(count());
        page.setItems(new ArrayList<>(size));
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement("select * from consumer limit ? offset ?;")) {
            pstmt.setInt(1, size);
            pstmt.setInt(2, page.getOffset());
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    page.getItems().add(rs2consumer(rs));
                }
            }
        }
        return page;
    }

    @Override
    public Consumer getById(Long id) throws Exception {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement("select * from consumer where consumer_id = ?;")) {
            pstmt.setLong(1, id);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    return rs2consumer(rs);
                } else return new Consumer();
            }
        } catch (SQLException e) {
            throw new Exception("Get user exception");
        }
    }

    private Consumer rs2consumer(ResultSet rs) throws SQLException {
        Consumer consumer = new Consumer();
        consumer.setId(rs.getLong("consumer_id"));
        consumer.setNickname(rs.getString("login"));
        consumer.setEmail(rs.getString("email"));
        consumer.setFirstName(rs.getString("first_name"));
        consumer.setSurname(rs.getString("surname"));
        consumer.setThirdName(rs.getString("third_name"));
        return consumer;
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        return false;
    }

    private String getSalt(Long id, String login) {
        try (PreparedStatement pstmt = dbProvider.get().prepareStatement("select salt from consumer where consumer_id = ? and login = ?")) {
            pstmt.setLong(1, id);
            pstmt.setString(2, login);

            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    return rs.getString(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean setNewPassword(Long id, String secret) {
        try {
            Consumer consumer = getById(id);
            if (consumer != null) {
                byte[] digester = getUserSecretDigest(consumer, secret);
                try (PreparedStatement pstmt = dbProvider.get().prepareStatement("update consumer set hash = ? where consumer_id = ? and login = ? and hash is null;")) {
                    pstmt.setBytes(1, digester);
                    pstmt.setLong(2, id);
                    pstmt.setString(3, consumer.getNickname());
                    return pstmt.executeUpdate() == 1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updatePassword(Long id, String newSecret, String oldSecret) {
        try {
            Consumer consumer = getById(id);
            if (consumer != null) {
                byte[] oldDigester = getUserSecretDigest(consumer, oldSecret);
                try (PreparedStatement pstmt = dbProvider.get().prepareStatement("select count(*) from consumer where consumer_id = ? and hash = ?;")) {
                    pstmt.setLong(1, id);
                    pstmt.setBytes(2, oldDigester);

                    try (ResultSet rs = pstmt.executeQuery()) {
                        if (!rs.next() || rs.getInt(1) != 1) {
                            return false;
                        }
                    }
                }

                byte[] digester = getUserSecretDigest(consumer, newSecret);
                try (PreparedStatement pstmt = dbProvider.get().prepareStatement("update consumer set hash = ? where consumer_id = ? and login = ?;")) {
                    pstmt.setBytes(1, digester);
                    pstmt.setLong(2, id);
                    pstmt.setString(3, consumer.getNickname());
                    return pstmt.executeUpdate() == 1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private byte[] getUserSecretDigest(Consumer consumer, String newSecret) throws NoSuchAlgorithmException {
        consumer.setSalt(getSalt(consumer.getId(), consumer.getNickname()));
        String forHash = "{" + String.join(",", String.valueOf(consumer.getId()), consumer.getNickname(), newSecret, consumer.getSalt()) + "}";
        MessageDigest digester = MessageDigest.getInstance("MD5");
        return digester.digest(forHash.getBytes());
    }

    static String abc = "12qwaszx34erdfcv56tyghbn78uijkm,90opl;./-=[]'!@#$%^&*()_+`~";
    public static String generateString(Random rng) {
        int length = 0;
        while (length < 5) {
            length = rng.nextInt(64);
        }
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = abc.charAt(rng.nextInt(abc.length()));
        }
        return new String(text);
    }

    public Consumer findBy(String nickname, String email) {
        try (PreparedStatement ps = dbProvider.get().prepareStatement("select * from consumer where login = ? or email = ?")) {
            if (nickname == null) {
                ps.setNull(1, Types.VARCHAR);
            } else {
                ps.setString(1, nickname);
            }
            if (email == null) {
                ps.setNull(2, Types.VARCHAR);
            } else {
                ps.setString(2, email);
            }
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs2consumer(rs);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
