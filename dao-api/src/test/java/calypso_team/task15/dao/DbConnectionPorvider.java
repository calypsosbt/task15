package calypso_team.task15.dao;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import org.flywaydb.core.Flyway;
import org.junit.Assert;

import javax.annotation.Priority;
import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;
import java.io.File;
import java.sql.*;

@Dependent @Priority(0)
public class DbConnectionPorvider {
    DataSource dataSource;

    @Produces @XADS
    DbConnection connectionProducer() throws ManagedProcessException {
        if (dataSource == null ){
            File target = new File("target", "db");
            if (!target.exists()) {
                if (!target.mkdirs()) {
                }
            }
            DBConfigurationBuilder configBuilder = DBConfigurationBuilder.newBuilder();
            configBuilder.setPort(3306);
            configBuilder.setDataDir(target.getAbsolutePath());
            configBuilder.setDatabaseVersion("mariadb-10.2.11");
            DB db = DB.newEmbeddedDB(configBuilder.build());
            db.start();
            db.createDB("main");

            Flyway flyway = new Flyway();
            String url = "jdbc:mysql://localhost:3306/main?characterEncoding=utf-8&useUnicode=true&serverTimezone=CTT";
            flyway.setDataSource(url, "root", "");
            flyway.setLocations("migration");
            flyway.clean();
            flyway.migrate();

            try (
                    Connection con = DriverManager.getConnection(url, "root", "");
                    PreparedStatement ps = con.prepareStatement("show tables;");
                    ResultSet rs = ps.executeQuery()
            ) {
                Assert.assertTrue(rs.next());
                return () -> con;
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    private class DbConnectionImpl implements DbConnection {
        public Connection get() throws SQLException {
            Connection connection = dataSource.getConnection();
//            connection.setAutoCommit(true);
            return connection;
        }
    }
}
