package calypso_team.task15.pm.acme;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.annotation.Resource;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.security.Principal;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Provider
public class JWTResponseFilter implements ContainerResponseFilter {

    @Resource(lookup = "java:app/AppName")
    private String applicationName;

    @Context
    UriInfo uri;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {

        String domain = uri.getBaseUri().getHost();
        if (
                requestContext.getSecurityContext() != null &&
                        requestContext.getSecurityContext().getUserPrincipal() != null
        ) {
            Cookie cookie = requestContext.getCookies().get("Authorization");
            if (cookie != null && cookie.getValue() != null) {
                String jwt = cookie.getValue();
                if (jwt != null && !jwt.isEmpty()) {
                    Claims claims = Jwts.parser()
                            .setSigningKey(JWTAuthFilter.signKey)
                            .parseClaimsJws(jwt).getBody();
                    if (claims.getExpiration() != null && claims.getExpiration().after(new Date())) {
                        return;
                    }
                }
            }
            Principal principal = requestContext.getSecurityContext().getUserPrincipal();
            if (principal instanceof JWTAuthFilter.UserPrincipal) {
                JWTAuthFilter.UserPrincipal mainPrince = (JWTAuthFilter.UserPrincipal) principal;
                long nowMillis = System.currentTimeMillis();
                Date now = new Date(nowMillis);
                String token = Jwts.builder().setId(String.valueOf(mainPrince.getUser().getId()))
                        .setIssuer(String.valueOf(mainPrince.getUser().getId()))
                        .setIssuedAt(now)
                        .setExpiration(Date.from(now.toInstant().plus(1, ChronoUnit.DAYS)))
                        .signWith(SignatureAlgorithm.HS512, JWTAuthFilter.signKey).compact();
                setCoockie(responseContext, token, domain);

            }
        }
    }

    private void setCoockie(ContainerResponseContext responseContext, String token, String domain) {
        responseContext.getHeaders().add(
                "Set-Cookie",
                new NewCookie(
                        "Authorization",
                        token,
                        "/" + applicationName,
                        domain, "jwt", 60*60, false
                ));
    }
}
