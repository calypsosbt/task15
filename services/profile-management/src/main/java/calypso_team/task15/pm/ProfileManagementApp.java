package calypso_team.task15.pm;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;

@ApplicationPath("pm")
public class ProfileManagementApp extends Application {
    public ProfileManagementApp() {

    }

    @Path("version") @GET @Produces("text/plain")
    public String version() {
        return "version";
    }
}
