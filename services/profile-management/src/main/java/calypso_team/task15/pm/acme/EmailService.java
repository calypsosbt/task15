package calypso_team.task15.pm.acme;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Stateless
public class EmailService {
    @Resource(lookup = "java:/mail/yandex")
    Session mailSession;

    public void sendConfirmation(String email, String url) throws MessagingException {
        sendMail(email, "Registration", "Confirm registration by url " + url);
    }

    public void sendMail(String email, String subject, String text) throws MessagingException {
        MimeMessage confirmation = new MimeMessage(mailSession);
        confirmation.setRecipients(Message.RecipientType.TO, new InternetAddress[] { new InternetAddress(email) });
        confirmation.setSubject(subject);
        confirmation.setContent(text, "text/plain");
        Transport.send(confirmation);
    }
}
