package calypso_team.task15.pm;

import calypso_team.task15.dao.LEntitiesDAO;
import calypso_team.task15.model.LegalEntities;
import calypso_team.task15.model.Page;
import calypso_team.task15.rest.LEService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.SQLException;

@Stateless
public class LEServiceImpl implements LEService {
    @Inject
    LEntitiesDAO lEntitiesDAO;

    @Override
    public LegalEntities get(Long id) {
        try {
            return lEntitiesDAO.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Long id, LegalEntities item) {
        try {
            lEntitiesDAO.update(id, item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(LegalEntities item) {
        try {
            lEntitiesDAO.create(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Long id) {
        try {
            lEntitiesDAO.delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Page<LegalEntities> filter(Page<LegalEntities> page) {
        return null;
    }
}
