package calypso_team.task15.pm;

import calypso_team.task15.dao.StatisticDAO;
import calypso_team.task15.model.Statistic;
import calypso_team.task15.rest.StatisticService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class StatisticImpl implements StatisticService {

    @Inject
    StatisticDAO statisticDAO;

    @Override
    public void insert(List<Statistic> items) {
        try {
            for(Statistic item : items) {
                statisticDAO.insert(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
