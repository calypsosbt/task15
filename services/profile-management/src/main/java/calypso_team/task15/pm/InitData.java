package calypso_team.task15.pm;

import calypso_team.task15.dao.ConsumerDAO;
import calypso_team.task15.db.MainDatabase;
import calypso_team.task15.model.Consumer;
import calypso_team.task15.pm.acme.EmailService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.mail.MessagingException;

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class InitData {
    @Inject
    DbConnectionPorvider dbConnectionPorvider;

    @Inject
    ConsumerDAO consumerDao;

    @Inject
    EmailService emailService;

    @PostConstruct
    void init() {
        MainDatabase.clean(dbConnectionPorvider.getDataSource());
        MainDatabase.migrate(dbConnectionPorvider.getDataSource());

        Consumer admin = new Consumer();
        admin.setNickname("admin");
        admin.setEmail("admin@admin");
        admin.setFirstName("admin");
        admin.setSurname("admin");
        admin.setThirdName("admin");
        try {
            consumerDao.create(admin);
            consumerDao.setNewPassword(admin.getId(), "new password");
            consumerDao.updatePassword(admin.getId(), "123", "new password");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            emailService.sendMail("wstarcev-ga@yandex.ru", "Service init data", "test message");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
