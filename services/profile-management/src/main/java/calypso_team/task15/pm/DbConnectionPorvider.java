package calypso_team.task15.pm;

import calypso_team.task15.dao.DbConnection;
import calypso_team.task15.dao.XADS;

import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Dependent
public class DbConnectionPorvider {
    @Resource(lookup = "java:jboss/datasources/task15")
    DataSource dataSource;

    @Produces @XADS
    DbConnection connectionProducer() {
        return new DbConnectionImpl();
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    private class DbConnectionImpl implements DbConnection {
        public Connection get() throws SQLException {
            Connection connection = dataSource.getConnection();
//            connection.setAutoCommit(true);
            return connection;
        }
    }
}
