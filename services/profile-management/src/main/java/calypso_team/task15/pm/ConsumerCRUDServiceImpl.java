package calypso_team.task15.pm;

import calypso_team.task15.dao.ConfirmationsDAO;
import calypso_team.task15.dao.ConsumerDAO;
import calypso_team.task15.model.ChangePassword;
import calypso_team.task15.model.Consumer;
import calypso_team.task15.model.Page;
import calypso_team.task15.pm.acme.EmailService;
import calypso_team.task15.rest.ConsumerCRUDService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;

@Stateless
public class ConsumerCRUDServiceImpl implements ConsumerCRUDService {
    @Inject
    ConsumerDAO consumerDao;

    @Inject
    ConfirmationsDAO confirmationsDAO;

    @Inject
    EmailService emailService;

    @Context
    UriInfo uri;

    public Consumer get(Long id) {
        try {
            return consumerDao.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void update(Long id, Consumer item) {

    }

    public void create(Consumer item) {
        try {
            consumerDao.create(item);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id) {

    }

    public Page<Consumer> filter(Page<Consumer> page) {
        try {
            return consumerDao.getPage(page.getPage(), page.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Consumer> page(Integer id) {
        try {
            return consumerDao.getPage(id, 25).getItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean register(Consumer consumer) {
        if (consumerDao.findBy(consumer.getNickname(), consumer.getEmail()) == null) {
            try {
                consumerDao.create(consumer);
                consumerDao.setNewPassword(consumer.getId(), consumer.getHash());
                String guid = confirmationsDAO.create(consumer.getId());
                if (guid != null) {
                    emailService.sendConfirmation(
                            consumer.getEmail(),
                            uri.getAbsolutePath().toURL().toString()
                                    + "?guid=" + guid
                                    + "&con_id=" + consumer.getId()
                    );
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public Response activate(Long id, String guid) {
        try {
            if (confirmationsDAO.markIfExists(id, guid)) {
                return Response.seeOther(URI.create("/..")).build();
            } else {
                return Response.noContent().build();
            }
        } catch (SQLException e) {
            return Response.serverError().entity("Ошибка активации").build();
        }
    }

    @Override
    public boolean setUserPassword(ChangePassword data) {
        return consumerDao.updatePassword(data.getId(), data.getNewPassword(), data.getOldPassword());
    }

    @Override
    public boolean setSecret(ChangePassword data) {
        return consumerDao.setNewPassword(data.getId(), data.getSecret());
    }
}
