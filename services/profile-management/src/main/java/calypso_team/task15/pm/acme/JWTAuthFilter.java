package calypso_team.task15.pm.acme;

import calypso_team.task15.dao.ConfirmationsDAO;
import calypso_team.task15.dao.ConsumerDAO;
import calypso_team.task15.model.Consumer;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import javax.annotation.Priority;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;

@Priority(Priorities.AUTHENTICATION)
@Provider
public class JWTAuthFilter implements ContainerRequestFilter {

    @Inject
    private ConsumerDAO securityDao;

    @Inject
    ConfirmationsDAO confirmationsDAO;

    @Context
    ResourceInfo info;

    static public String signKey = "sign key;";
    private static Response.ResponseBuilder error = Response.status(401);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        PermitAll permitAll = info.getResourceMethod().getAnnotation(PermitAll.class);
        if (permitAll != null) {
            return;
        }
        Cookie cookie = requestContext.getCookies().get("Authorization");
        if (cookie == null) {
            exit(requestContext, "not authorized");
            return;
        }
        try {
            Consumer user = new ObjectMapper().readValue(requestContext.getEntityStream(), Consumer.class);
            assert user != null;
            Consumer inbaseUser = securityDao.getById(user.getId());
            assert inbaseUser != null;
            assert confirmationsDAO.isActive(inbaseUser.getId());
            requestContext.setSecurityContext(JWTAuthFilter.getContext(inbaseUser));
            byte[] bytes = new ObjectMapper().writerFor(Consumer.class).writeValueAsBytes(inbaseUser);
            requestContext.setEntityStream(new ByteArrayInputStream(bytes));
        } catch (Throwable e) {
            exit(requestContext, "not authorized");
            return;
        }

        String jwt = cookie.getValue();

        if (jwt == null || jwt.trim().isEmpty()) {
            exit(requestContext, "jwt is empty");
            return;
        }

        Claims claims = Jwts.parser()
                .setSigningKey(signKey)
                .parseClaimsJws(jwt).getBody();

        if (claims.isEmpty()) {
            exit(requestContext, "jwt is empty");
            return;
        }
        else if (claims.getExpiration() != null && claims.getExpiration().before(new Date())) {
            exit(requestContext, "jwt expired");
            return;
        }

        try {

            Consumer user = new Consumer(); //securityDao.getUserById(Long.valueOf(claims.getId()));
            user.setId(Long.valueOf(claims.getIssuer()));

            user = securityDao.getById(user.getId());

            if (user == null) {
                exit(requestContext, "user not found");
                return;
            }


//            if (!user.isActive()) {
//                log.error("user not active: {}#{}", user.getId(), user.getEmail());
//                exit(requestContext, "user not active");
//                return;
//            }

//            log.info("user {} is logged in", user.getEmail());

            requestContext.setSecurityContext(JWTAuthFilter.getContext(user));


        } catch (Error e) {
//            log.error("auth error", e);
            exit(requestContext, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exit(ContainerRequestContext requestContext, String message) {
        requestContext.abortWith(error.entity(message).build());
    }

    public static SecurityContext getContext(Consumer user) {
        return new SecurityContext() {
            UserPrincipal up = new UserPrincipal(user);
            @Override
            public Principal getUserPrincipal() {
                return up;
            }

            @Override
            public boolean isUserInRole(String role) {
                return true;
            }

            @Override
            public boolean isSecure() {
                return true;
            }

            @Override
            public String getAuthenticationScheme() {
                return null;
            }
        };
    }

    public static class UserPrincipal implements Principal {
        Consumer user;
        public UserPrincipal(Consumer user) {
            this.user = user;
        }

        @Override
        public String getName() {
            return user.getNickname();
        }

        public Consumer getUser() {
            return user;
        }
    }
}
