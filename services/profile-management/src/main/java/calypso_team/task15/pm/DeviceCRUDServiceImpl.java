package calypso_team.task15.pm;

import calypso_team.task15.model.Device;
import calypso_team.task15.model.Page;
import calypso_team.task15.rest.DeviceCRUDService;

import javax.ejb.Stateless;

@Stateless
public class DeviceCRUDServiceImpl implements DeviceCRUDService {
    public Device get(Long id) {
        return null;
    }

    public void update(Long id, Device item) {

    }

    public void create(Device item) {

    }

    @Override
    public void delete(Long id) {

    }

    public void update(Long id) {

    }

    public Page<Device> filter(Page<Device> page) {
        return null;
    }
}
